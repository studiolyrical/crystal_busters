﻿namespace crystal_busters
{
    public class UnitEntityComponent : ILyricalComponent
    {
        #region Default Constructor
        public UnitEntityComponent()
        {
            InstanceID = 0;
            Class = UnitClass.NULL;
        }
        #endregion

        #region Copy Constructor
        public UnitEntityComponent(Entity bEntity)
            : this()
        {
            Class = bEntity.UnitComponent.Class;
            HP = bEntity.UnitComponent.HP;
        }
        #endregion

        #region Properties
        public int InstanceID { get; set; }
        public UnitClass Class { get; set; }
        public int HP { get; set; }
        #endregion
    }
}