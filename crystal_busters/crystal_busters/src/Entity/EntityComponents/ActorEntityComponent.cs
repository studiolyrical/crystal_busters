﻿#region Using Statements
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
#endregion

namespace crystal_busters
{
    public class ActorEntityComponent : ILyricalComponent
    {
        #region Default Constructor
        public ActorEntityComponent()
        {
            InstanceID = 0;
            Hurtbox = new Rectangle();
            HurtboxOffset = new Vector2();
        }
        #endregion

        #region Copy Constructor
        public ActorEntityComponent(Entity bEntity)
            : this()
        {
            Hurtbox = bEntity.ActorComponent.Hurtbox;
            HurtboxOffset = bEntity.ActorComponent.HurtboxOffset;
        }
        #endregion

        #region Properties
        public int InstanceID { get; set; }

        [JsonProperty, JsonConverter(typeof (XNARectangleConverter))]
        public Rectangle Hurtbox { get; set; }

        public Vector2 HurtboxOffset { get; set; }
        #endregion
    }
}