﻿#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace crystal_busters
{
    public class PositionEntityComponent : ILyricalComponent
    {
        #region Default Constructor
        public PositionEntityComponent()
        {
            InstanceID = 0;
            Position = new Vector2();
            GridEntity = false;
        }
        #endregion

        #region CopyConstructor
        public PositionEntityComponent(Entity bEntity)
            : this()
        {
            Position = bEntity.PositionComponent.Position;
            GridEntity = bEntity.PositionComponent.GridEntity;
        }
        #endregion

        #region Properties
        public int InstanceID { get; set; }
        public Vector2 Position { get; set; } // Entity's world coordinates
        public bool GridEntity { get; set; } // Does this entity snap to grid?
        #endregion
    }
}