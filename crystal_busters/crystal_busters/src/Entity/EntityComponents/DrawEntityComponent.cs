﻿#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace crystal_busters
{
    public class DrawEntityComponent : ILyricalComponent
    {
        #region Default Constructor
        public DrawEntityComponent()
        {
            InstanceID = 0;
            Scale = new Point();
            TintColor = new Color();
            Flash = false;
            Rotation = 0.0f;
            Origin = new Vector2();
            Flip = SpriteEffects.None;
            Layer = 0.0f;
            ParallaxLayer = 0;
            CurrentSprite = new Sprite();
            NextSprite = new Sprite();
            IdleSprite = new Sprite();
        }
        #endregion

        #region Copy Constructor
        public DrawEntityComponent(Entity bEntity)
            : this()
        {
            Scale = bEntity.DrawComponent.Scale;
            TintColor = bEntity.DrawComponent.TintColor;
            Flash = bEntity.DrawComponent.Flash;
            Rotation = bEntity.DrawComponent.Rotation;
            Origin = bEntity.DrawComponent.Origin;
            Flip = bEntity.DrawComponent.Flip;
            Layer = bEntity.DrawComponent.Layer;
            ParallaxLayer = bEntity.DrawComponent.ParallaxLayer;

            CurrentSprite = new Sprite(bEntity.DrawComponent.IdleSprite);
            NextSprite = new Sprite(CurrentSprite);
            IdleSprite = new Sprite(bEntity.DrawComponent.IdleSprite);
            //if (bEntity.DrawComponent.WalkSprite != null)
            //    WalkSprite = new Sprite(bEntity.DrawComponent.WalkSprite);   
        }
        #endregion

        #region Properties
        public int InstanceID { get; set; }
        public Point Scale { get; set; } // X,Y Scaling
        public Color TintColor { get; set; } // RGBA
        public bool Flash { get; set; }
        public float Rotation { get; set; }
        public Vector2 Origin { get; set; } // Sprite origin point for rotation operations
        public SpriteEffects Flip { get; set; } // None, FlipHorizontally, FlipVertically
        public float Layer { get; set; }
        public int ParallaxLayer { get; set; } // Parallax layer, corresponds to value in scene_data

        public Sprite CurrentSprite { get; set; }
        public Sprite NextSprite { get; set; }
        public Sprite IdleSprite { get; set; }
        //public Sprite WalkSprite { get; set; }
        #endregion
    }
}