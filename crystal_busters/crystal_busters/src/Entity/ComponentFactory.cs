﻿namespace crystal_busters
{
    public class ComponentFactory
    {
        #region Default Constructor
        public ComponentFactory()
        {
            _idCounter = 0;
        }
        #endregion

        #region Fields
        private int _idCounter;
        #endregion

        #region Public Functions
        public void CopyComponents(Entity baseEntity, Entity newEntity)
        {
            if (baseEntity.PositionComponent != null) {
                newEntity.PositionComponent = new PositionEntityComponent(baseEntity);
                AssignID(newEntity.PositionComponent);
            }
            if (baseEntity.DrawComponent != null) {
                newEntity.DrawComponent = new DrawEntityComponent(baseEntity);
                AssignID(newEntity.DrawComponent);
            }
            if (baseEntity.ActorComponent != null) {
                newEntity.ActorComponent = new ActorEntityComponent(baseEntity);
                AssignID(newEntity.ActorComponent);
            }
            if (baseEntity.UnitComponent != null) {
                newEntity.UnitComponent = new UnitEntityComponent(baseEntity);
                AssignID(newEntity.UnitComponent);
            }
        }
        #endregion

        #region Private Functions
        private void AssignID(ILyricalComponent component)
        {
            component.InstanceID = _idCounter;
            _idCounter++;
        }
        #endregion

#if DEBUG
        //public void DebugCreateComponent(ILyricalComponent baseComponent, Entity ent,
        //    Entity bEntity = null)
        //{
        //    if (baseComponent.GetType() == typeof (PositionEntityComponent)) {
        //        ent.PositionComponent = bEntity != null
        //            ? new PositionEntityComponent(bEntity) : new PositionEntityComponent();
        //        ent.PositionComponent.InstanceID = _idCounter;
        //    } else if (baseComponent.GetType() == typeof (DrawEntityComponent)) {
        //        ent.DrawComponent = bEntity != null
        //            ? new DrawEntityComponent(bEntity) : new DrawEntityComponent();
        //        ent.DrawComponent.InstanceID = _idCounter;
        //    } else if (baseComponent.GetType() == typeof (ActorEntityComponent)) {
        //        ent.ActorComponent = bEntity != null
        //            ? new ActorEntityComponent(bEntity) : new ActorEntityComponent();
        //    } else {
        //        LyricalDebug.ExceptionMessage(
        //            "You're trying to make a component type that doesn't exist.");
        //        Debugger.Break();
        //    }

        //    _idCounter++;
        //}
#endif
    }
}