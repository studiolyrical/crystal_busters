﻿namespace crystal_busters
{
    public class Entity
    {
        #region Default Constructor
        public Entity()
        {
            Name = string.Empty;
            InstanceID = 0;
            RemoveMeFromList = false;
            PositionComponent = null;
            DrawComponent = null;
            ActorComponent = null;
            UnitComponent = null;
        }
        #endregion

        #region Properties
        public string Name { get; set; }
        public int InstanceID { get; set; }
        public bool RemoveMeFromList { get; set; }

        // Componenets
        public PositionEntityComponent PositionComponent { get; set; }
        public DrawEntityComponent DrawComponent { get; set; }
        public ActorEntityComponent ActorComponent { get; set; }
        public UnitEntityComponent UnitComponent { get; set; }
        #endregion
    }
}