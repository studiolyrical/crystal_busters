﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
#endregion

/* ----------------------------------------------------------------------------------
 * Description:
 * ● This is a factory that also maintains a list of objects it creates
 * ● Deserializes entity_data, keeps a map of all entities, spawns entities
 * ---------------------------------------------------------------------------------- */

namespace crystal_busters
{
    public class EntityManager
    {
        #region Default Constructor
        public EntityManager()
        {
            _componentFactory = new ComponentFactory();
            _entityMap =
                JsonConvert.DeserializeObject<Dictionary<int, Entity>>(
                    File.ReadAllText("data/entity_data.lyr"));
            _activeEntities = new List<Entity>();
            _entityIDCounter = 0;
        }
        #endregion

        #region Fields
        private readonly ComponentFactory _componentFactory;
        private readonly Dictionary<int, Entity> _entityMap; // Entity lookup table
        private readonly List<Entity> _activeEntities; // Keep track of all instantiated entities
        private int _entityIDCounter; // Unique instance ID counter
        #endregion

        #region Properties
        public List<Entity> ActiveEntities { get { return _activeEntities; } }
        #endregion

        #region Public Functions
        // Instantiates a single Entity object
        public Entity SpawnEntity(int entID = 0, Entity bEntity = null)
        {
            var baseEntity = new Entity();

            if (bEntity != null) { // Is baseEntity a clone passed in as an argument,
                baseEntity = bEntity;
            } else { // or do we fall back to our main entityMap?
                try {
                    baseEntity = _entityMap[entID];
                } catch (Exception) {
                    LyricalDebug.ExceptionMessage(
                        "Can't spawn something from entity_data.lyr, probably the " +
                        "ID is invalid.\n ID = " + entID);
                }
            }

            var ent = new Entity {InstanceID = _entityIDCounter};
            _entityIDCounter++;

            try {
                ent.Name = string.Copy(baseEntity.Name);
            } catch (Exception) {
                LyricalDebug.ExceptionMessage("Entity (Instance ID #" + ent.InstanceID +
                                              ") needs a name.");
                Debugger.Break();
            }

            _componentFactory.CopyComponents(baseEntity, ent);
            _activeEntities.Add(ent); // add to master list

            return ent;
        }

        // Function for spawning entities when a scene initializes, based on a list that the scene holds
        public void SpawnEntitiesFromList(List<Entity> entities)
        {
            foreach (Entity ent in entities) {
                SpawnEntity(0, ent);
            }
        }

        // Call this everytime you want to remove an entity from the game
        public void MarkEntityForRemoval(int entInstanceID)
        {
            foreach (Entity ent in _activeEntities) {
                if (ent.InstanceID == entInstanceID) {
                    ent.RemoveMeFromList = true;
                }
            }
        }
        #endregion

#if DEBUG
        public void ClearAllActiveEntities()
        {
            _activeEntities.Clear();
        }

        public int EntityIDCounter { get { return _entityIDCounter; } }
        public void SetEntityIDCounter(int n)
        {
            _entityIDCounter = n;
        }
#endif
    }
}