﻿// ReSharper disable InconsistentNaming
namespace crystal_busters
{
    public enum SceneTransitionState
    {
        TransitionOn, // Scene is transitioning on
        Active, // Scene is Active (accepts input)
        TransitionOff, // Scene is transitioning off
        Hidden // Scene is hidden; not deleted, but not drawn or active
    }

    public enum UnitClass
    {
        NULL,
        PLDN, // Paladin
        CLRC, // Cleric
        MONK, // Monk
        PLSP, // Philosopher
        TRFM, // Terraformer
        ARCH, // Archer
        DNCR, // Dancer
        WTCH, // Witch
        SNGM, // Shinigami
        MIKO, // Miko
        KUNO, // Kunoichi
        WEAV // Weaver
    }

    public enum EventUI
    {
        TEST,
        ATTACK
    }
}