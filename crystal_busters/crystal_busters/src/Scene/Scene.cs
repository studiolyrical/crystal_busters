﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VosSoft.Xna.GameConsole;
#endregion

namespace crystal_busters
{
    /* ----------------------------------------------------------------------------------
     * Description:
     * ● Scenes contain entities (game objects). Transitioning between Scenes would 
     * imply transitioning between game states. For example, you could go from the 
     * Main Menu Scene to the Gameplay Scene.
     * ● I don't expect to need composition for the different types of Scenes, so instead
     * this is an abstract base class which other Scene-type classes will derive from.
     * ● A lot of this approach is lifted from Microsoft's GSM implementation here:
     * http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
     * ---------------------------------------------------------------------------------- */

    public abstract class Scene
    {
        protected Scene()
        {
            TransitionOnTime = TimeSpan.Zero;
            TransitionOffTime = TimeSpan.Zero;
            TransitionPosition = 0.0f;
            Exiting = false;
            ControllingPlayer = new PlayerIndex();
            Name = string.Empty;
            SceneState = SceneTransitionState.Hidden;
            TransitionOnTimeInt = 0;
            TransitionOffTimeInt = 0;
            RemoveMeFromList = false;
            SceneCamera = new Camera();
            CamPos = Vector2.Zero;
            CamZoom = 0;
            CamRotation = 0;
            CamBoundaryMin = Vector2.Zero;
            CamBoundaryMax = Vector2.Zero;
            ParallaxLayers = new List<Vector2>();
            InstanceID = 0;
        }

        #region Properties
        protected TimeSpan TransitionOnTime { get; set; }
        protected TimeSpan TransitionOffTime { get; set; }
        protected float TransitionPosition { get; set; }
        protected bool Exiting { get; set; } // If set, Scene begins to TransitionOff
        protected PlayerIndex? ControllingPlayer { get; set; }
        public string Name { get; set; }
        public SceneTransitionState SceneState { get; protected set; }
        public int TransitionOnTimeInt { get; set; } // TransitionOnTime in milliseconds
        public int TransitionOffTimeInt { get; set; } // TransitionOffTime in milliseconds
        public bool RemoveMeFromList { get; private set; }
        public Camera SceneCamera { get; private set; }
        public Vector2 CamPos { get; set; }
        public float CamZoom { get; set; }
        public float CamRotation { get; set; }
        public Vector2 CamBoundaryMin { get; set; }
        public Vector2 CamBoundaryMax { get; set; }
        public List<Vector2> ParallaxLayers { get; set; }
        public int InstanceID { get; set; }
        #endregion

        #region Initialization
        protected virtual void Initialize()
        {
            SetupCamera();

#if DEBUG
            AddConsoleCommands();
#endif
        }
        #endregion

        #region Update Loops
        public virtual void Update(GameTime gameTime, InputHandler inputHandler)
        {
            if (Exiting) {
                SceneState = SceneTransitionState.TransitionOff;

                if (!UpdateTransition(gameTime, TransitionOffTime, 1)) {
                    RemoveMeFromList = true;
                }
            } else {
                SceneState = UpdateTransition(gameTime, TransitionOnTime, -1)
                    ? SceneTransitionState.TransitionOn : SceneTransitionState.Active;
            }
        }

        public abstract void FixedUpdate();

        public abstract void Draw(GameTime gameTime, GraphicsDevice graphicsDevice,
            SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            Dictionary<string, SpriteFont> fontContent);
        #endregion

        #region Protected Functions
        protected void FadeBackBufferToBlack(float alpha, Viewport viewport, SpriteBatch spriteBatch)
        {
            var texContent = GameServices.GetService<Dictionary<string, Texture2D>>();

            spriteBatch.Begin();

            spriteBatch.Draw(texContent["blank"],
                new Rectangle(0, 0, viewport.Width, viewport.Height), Color.Black * alpha);

            spriteBatch.End();
        }

        protected void SetupCamera()
        {
            var camFactory = new CameraFactory();

            SceneCamera = camFactory.CreateCamera(CamPos, CamZoom, CamRotation, CamBoundaryMin,
                CamBoundaryMax);
        }
        #endregion

        #region Private Functions
        // Updates _transitionPosition, returns true if a Scene is still transitioning
        private bool UpdateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            float transitionDelta;

            if (time == TimeSpan.Zero) {
                transitionDelta = 1;
            } else {
                transitionDelta =
                    (float)(gameTime.ElapsedGameTime.TotalMilliseconds / time.TotalMilliseconds);
            }

            TransitionPosition += transitionDelta * direction;

            // Did we hit the end of the transition?
            if ((direction < 0) && (TransitionPosition <= 0) ||
                (direction > 0) && TransitionPosition >= 1) {
                TransitionPosition = MathHelper.Clamp(TransitionPosition, 0, 1);
                return false;
            }

            // If not, continue transitioning
            return true;
        }

#if DEBUG
        private void AddConsoleCommands()
        {
            var devConsole = GameServices.GetService<GameConsole>();
            devConsole.RemoveCommand("mc");

            devConsole.AddCommand("mc", delegate {
                var inputHandler = GameServices.GetService<ILyricalInputHandlerService>();
                LyricalDebug.Log(inputHandler.GetMousePosition(SceneCamera).ToString());
            });
        }
#endif 
        #endregion
    }
}