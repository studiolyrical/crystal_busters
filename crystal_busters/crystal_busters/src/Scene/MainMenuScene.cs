﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace crystal_busters
{
    // just a test class atm
    public class MainMenuScene : Scene
    {
        #region Default Constructor
        public MainMenuScene() {}
        #endregion

        #region Copy Constructors
        public MainMenuScene(MainMenuScene bMainMenuScene)
        {
            _bMainMenuScene = bMainMenuScene;
            Name = bMainMenuScene.Name;
            TransitionOffTime = TimeSpan.FromMilliseconds(bMainMenuScene.TransitionOffTimeInt);
        }
        #endregion

        #region Fields
        private readonly MainMenuScene _bMainMenuScene;
        #endregion

        #region Properties
        #endregion

        #region Update Loops
        public override void Update(GameTime gameTime, InputHandler inputHandler)
        {
            base.Update(gameTime, inputHandler);

            if (SceneState == SceneTransitionState.Active) {
                if (inputHandler.PressOnceKB(Keys.NumPad1)) {
                    var scnManager = GameServices.GetService<ILyricalSceneManagerService>();
                    scnManager.AddScene(new MapScene(1));
                    Exiting = true;
                }
            }
        }

        public override void FixedUpdate() {}

        public override void Draw(GameTime gameTime, GraphicsDevice graphicsDevice,
            SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            Dictionary<string, SpriteFont> fontContent)
        {
            graphicsDevice.Clear(Color.DarkBlue);

            spriteBatch.Begin();
            spriteBatch.DrawString(fontContent["devtext"], "CRYSTAL? BUSTERS! rev0.01",
                new Vector2(15, 15), Color.White);
            spriteBatch.End();
        }
        #endregion
    }
}