﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using VosSoft.Xna.GameConsole;
#endregion

namespace crystal_busters
{
    public sealed class MapScene : Scene
    {
        #region Default Constructor
        public MapScene()
        {
            _entManager = new EntityManager();
            _drawSystem = new DrawSystem();
            _actorSystem = new ActorSystem();
            _unitSystem = new UnitSystem();
            _uiAdapter = new MapSceneUIAdapter();
            _cells = new List<Rectangle>();
            _layerValues = new Dictionary<Point, float>();
            EntityCollection = new List<Entity>();
            MainGridRectangleVector4 = new Vector4();
            PlayerGridOffset = 0;
            PlayerGridXY = new Point();
        }
        #endregion

        #region Parameterized Constructors
        public MapScene(int mapID)
            : this()
        {
#if DEBUG
            var mapSceneLUT =
                JsonConvert.DeserializeObject<Dictionary<int, MapScene>>(
                    File.ReadAllText(LyricalDebug.DebugConfig.MapDataLocation));
            LyricalDebug.DebugConfig.LastMapSaveID = mapID;

            EntityIDCounterDebug = mapSceneLUT[mapID].EntityIDCounterDebug;
#else
            var mapSceneLUT =
                JsonConvert.DeserializeObject<Dictionary<int, MapScene>>(
                    File.ReadAllText("data/map.lyr"));
#endif


            Name = mapSceneLUT[mapID].Name;
            TransitionOnTime = TimeSpan.FromMilliseconds(mapSceneLUT[mapID].TransitionOnTimeInt);
            TransitionOnTime = TimeSpan.FromMilliseconds(mapSceneLUT[mapID].TransitionOffTimeInt);
            CamPos = mapSceneLUT[mapID].CamPos;
            CamZoom = mapSceneLUT[mapID].CamZoom;
            CamRotation = mapSceneLUT[mapID].CamRotation;
            CamBoundaryMin = mapSceneLUT[mapID].CamBoundaryMin;
            CamBoundaryMax = mapSceneLUT[mapID].CamBoundaryMax;
            ParallaxLayers = mapSceneLUT[mapID].ParallaxLayers;

            EntityCollection = mapSceneLUT[mapID].EntityCollection;
            MainGridRectangleVector4 = mapSceneLUT[mapID].MainGridRectangleVector4;
            PlayerGridOffset = mapSceneLUT[mapID].PlayerGridOffset;
            PlayerGridXY = mapSceneLUT[mapID].PlayerGridXY;

            Initialize();
        }
        #endregion

        #region Fields
        private readonly EntityManager _entManager;
        private readonly DrawSystem _drawSystem;
        private readonly ActorSystem _actorSystem;
        private readonly UnitSystem _unitSystem;
        private readonly MapSceneUIAdapter _uiAdapter;
        private readonly List<Rectangle> _cells;
        private readonly Dictionary<Point, float> _layerValues;
        #endregion

        #region Properties
        public List<Entity> EntityCollection { get; private set; }
        public Vector4 MainGridRectangleVector4 { get; set; }
        public int PlayerGridOffset { get; set; } // distance between the player's "subgrids"
        public Point PlayerGridXY { get; set; } // # of tiles we should have on X/Y for each grid
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            _entManager.SpawnEntitiesFromList(EntityCollection);
            SetupPlayerGrid();
#if DEBUG
            AddConsoleCommands();
            _entManager.SetEntityIDCounter(EntityIDCounterDebug);
#endif
            base.Initialize();
        }
        #endregion

        #region Update Loops
        public override void Update(GameTime gameTime, InputHandler inputHandler)
        {
            var entCleaning = new List<Entity>(_entManager.ActiveEntities);
            foreach (Entity ent in entCleaning) {
                if (ent.RemoveMeFromList) {
                    _entManager.ActiveEntities.Remove(ent);
                }
            }
            var actors = new List<Entity>();
            var units = new List<Entity>();
            var entitiesToDraw = new List<Entity>();

            foreach (Entity ent in _entManager.ActiveEntities) {
                if (ent.DrawComponent != null) {
                    entitiesToDraw.Add(ent);
                    if (_layerValues.ContainsKey(LyricalHelper.Vector2ToPoint(ent.PositionComponent.Position)))
                    {

                        if (ent.Name == "BigCrystal") {
                            ent.DrawComponent.Layer = 0.0007f;
                        } else {
                            ent.DrawComponent.Layer =
                            _layerValues[
                                LyricalHelper.Vector2ToPoint(ent.PositionComponent.Position)];
                        }
                    }
                }
                if (ent.PositionComponent != null && ent.ActorComponent != null) {
                    actors.Add(ent);
                    if (ent.UnitComponent != null) {
                        units.Add(ent);
                    }
                }
            }

            if (ParallaxLayers.Count > 0) {
                _drawSystem.Update(entitiesToDraw, ParallaxLayers.Count);
            }
            _actorSystem.Update(inputHandler, actors, SceneCamera);
            _uiAdapter.Update(_actorSystem.HoverActor, _actorSystem.SelectedUnit);
            base.Update(gameTime, inputHandler);
        }

        public override void FixedUpdate()
        {
            var entitiesToDraw = new List<Entity>();
            var actors = new List<Entity>();
            var units = new List<Entity>();

            foreach (Entity ent in _entManager.ActiveEntities) {
                if (ent.DrawComponent != null) {
                    entitiesToDraw.Add(ent);
                }

                if (ent.PositionComponent != null) {
                    if (ent.ActorComponent != null) {
                        actors.Add(ent);
                        if (ent.UnitComponent != null) {
                            units.Add(ent);
                        }
                    }
                }

                foreach (Rectangle cell in _cells) {
                    if (ent.PositionComponent != null && ent.PositionComponent.GridEntity &&
                        cell.Contains(new Point((int)ent.PositionComponent.Position.X,
                            (int)ent.PositionComponent.Position.Y))) {
                        ent.PositionComponent.Position = new Vector2(cell.Center.X, cell.Center.Y);
                    }
                }
            }
            _actorSystem.FixedUpdate();
            _drawSystem.FixedUpdate(entitiesToDraw);
        }

        public override void Draw(GameTime gameTime, GraphicsDevice graphicsDevice,
            SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            Dictionary<string, SpriteFont> fontContent)
        {
            graphicsDevice.Clear(Color.CornflowerBlue);
#if DEBUG
            if (LyricalDebug.DebugConfig.BackgroundVisible) {
                spriteBatch.Begin();
                spriteBatch.Draw(textureContent["bg"],
                    new Rectangle(0, 0, textureContent["bg"].Width, textureContent["bg"].Height),
                    Color.White);
                spriteBatch.End();
            }

            var lyricalDebug = GameServices.GetService<ILyricalDebugService>();
            lyricalDebug.DebugDrawBottom(spriteBatch, textureContent, this, _cells);
#endif

            _drawSystem.Draw(spriteBatch, textureContent, SceneCamera, ParallaxLayers);

#if DEBUG
            lyricalDebug.DebugDrawTop(spriteBatch, textureContent, fontContent, this,
                _entManager.ActiveEntities);
#endif
            _uiAdapter.Draw();
            if (SceneState != SceneTransitionState.TransitionOn) {
                return;
            }
            float alpha = MathHelper.Lerp(TransitionPosition, 1.0f, 0);

            FadeBackBufferToBlack(alpha, graphicsDevice.Viewport, spriteBatch);
        }
        #endregion

        #region Private Functions
        private void SetupPlayerGrid()
        {
            int offsetY = 0;
            int cellHeight = 0;
            // This loop creates a Rectangle (grid cell)
            for (int row = 0; row < PlayerGridXY.X; row++) {
                // Everytime a new row is started, reset offsetX, so the X position goes back to neutral
                int offsetX = 0;
                // When row increments, offset the Y position based on the previous created cell's height
                offsetY += cellHeight;
                for (int column = 0; column < PlayerGridXY.Y * 2; column++) {
                    int cellX = 0;
                    if (column >= PlayerGridXY.Y) { // Quick check to see if we're making P2's cells or not
                        // If we are, we need to include the PlayerGridOffset value for the X position
                        // and, we need slightly different logic for sprite ordering
                        cellX = (int)MainGridRectangleVector4.X + offsetX + PlayerGridOffset;
                    } else {
                        // Otherwise we're on P1 and can just keep X aligned with the left side of the main grid
                        cellX = (int)MainGridRectangleVector4.X + offsetX;
                    }

                    // The cell width size here is a little odd, we need to divide the main grid's size by the number of
                    // columns, and divide that by 2 to accomodate for both players. Then we need to worry about 
                    // subtracting the offset.
                    // PlayerGridOffset represents the TOTAL distance between each player grid, so we divide it by 2
                    // so that the offset only occurs in the middle of the field; and then we divide that result by 
                    // the number of columns each player has.
                    var cell = new Rectangle(cellX, (int)MainGridRectangleVector4.Y + offsetY,
                        (((int)MainGridRectangleVector4.Z / PlayerGridXY.Y / 2) -
                         ((PlayerGridOffset / 2) / PlayerGridXY.Y)),
                        (int)MainGridRectangleVector4.W / PlayerGridXY.X);

                    // determine the sprite layer in any given position
                    float layer = row * (PlayerGridXY.Y * 2) + Math.Abs(((PlayerGridXY.Y * 2) / 2) - column);
                    layer = layer * 0.0001f;
                    _layerValues.Add(cell.Center, layer);

                    _cells.Add(cell);
                    offsetX += cell.Width;
                    cellHeight = cell.Height;
                }
            }

            foreach (KeyValuePair<Point, float> kvp in _layerValues) {
                Console.WriteLine(kvp.Key + " " + kvp.Value);
            }
        }
        #endregion

#if DEBUG
        public int EntityIDCounterDebug;

        private void AddConsoleCommands()
        {
            var devConsole = GameServices.GetService<GameConsole>();

            // remove the commands of the previous scene if they exist
            devConsole.RemoveCommand("get");
            devConsole.RemoveCommand("set");
            devConsole.RemoveCommand("rm");
            devConsole.RemoveCommand("spawn");
            devConsole.RemoveCommand("savestate");
            devConsole.RemoveCommand("reassign");
            devConsole.RemoveCommand("ZENRYOKU_ZENKAI");

            // ghetto is as ghetto does, but dev debug code is dev debug code
            devConsole.AddCommand("get", delegate(object obj, CommandEventArgs e) {
                var lyricalDebug = GameServices.GetService<ILyricalDebugService>();

                if (e.Args.Length > 0) {
                    int number;
                    bool result = int.TryParse(e.Args[0], out number);
                    if (result) { // if the user passed an int, let's say they want an instance ID
                        List<Entity> l = // Make a list containing just the 1 entity
                            _entManager.ActiveEntities.Where(ent => ent.InstanceID == number).ToList
                                ();
                        if (e.Args.Length >= 2) {
                            int n;
                            bool r = int.TryParse(e.Args[1], out n);
                            if (r) {
                                lyricalDebug.PrintEntities(l, n);
                            } else if (e.Args[1] == "p") {
                                lyricalDebug.PrintEntities(l, true, true);
                                // pass list we made                                
                            }
                        } else { // or just componenets?
                            lyricalDebug.PrintEntities(l, true);
                        }
                    } else {
                        switch (e.Args[0]) {
                            case "component":
                            case "c":
                                if (e.Args.Length >= 2 && e.Args[1] == "properties" ||
                                    e.Args.Length >= 2 && e.Args[1] == "p") {
                                    lyricalDebug.PrintEntities(_entManager.ActiveEntities, true,
                                        true);
                                } else {
                                    lyricalDebug.PrintEntities(_entManager.ActiveEntities, true);
                                }
                                break;
                            case "cp":
                                lyricalDebug.PrintEntities(_entManager.ActiveEntities, true, true);
                                break;
                        }
                    }
                } else {
                    lyricalDebug.PrintEntities(_entManager.ActiveEntities);
                }
            }, "Print entity information.", "get [component|c] [cp]");

            devConsole.AddCommand("set", delegate(object obj, CommandEventArgs e) {
                foreach (Entity ent in _entManager.ActiveEntities) {
                    if (ent.InstanceID == int.Parse(e.Args[0])) {
                        IEnumerable<ILyricalComponent> componentList =
                            LyricalDebug.CheckEntityComponents(ent);

                        foreach (ILyricalComponent component in componentList) {
                            if (component.InstanceID == int.Parse(e.Args[1])) {
                                foreach (PropertyInfo p in
                                    component.GetType().GetProperties().Where(
                                        p => !p.GetGetMethod().GetParameters().Any())) {
                                    if (p.Name == e.Args[2]) {
                                        TypeConverter tc =
                                            TypeDescriptor.GetConverter(p.PropertyType);
                                        object value = tc.ConvertFromString(null,
                                            CultureInfo.InvariantCulture, e.Args[3]);
                                        p.SetValue(component, value, null);
                                    }
                                }
                            }
                        }
                    }
                }
            }, "Change values for components on entities",
                "set <entity_instanceID> <componentID> <propertyName> <value>");

            devConsole.AddCommand("spawn", delegate(object obj, CommandEventArgs e) {
                if (e.Args.Length > 0) {
                    if (e.Args[0] == "0") {
                        LyricalDebug.Log("dont spawn missingno");
                        return;
                    }
                    _entManager.SpawnEntity(int.Parse(e.Args[0]));
                } else {
                    devConsole.ExecManual("spawn");
                }
            }, "Entity ID can be found in entity_data.lyr", "spawn <entityID>");

            devConsole.AddCommand("rm", delegate(object obj, CommandEventArgs e) {
                if (e.Args.Length > 0) {
                    for (int i = 0; i < e.Args.Length; i++) {
                        foreach (Entity ent in _entManager.ActiveEntities) {
                            if (ent.InstanceID == int.Parse(e.Args[i])) {
                                _entManager.MarkEntityForRemoval(ent.InstanceID);
                            }
                        }
                    }
                }
            }, "Remove an Entity", "rm <entity_instanceID>");


            // this is seriously the worst thing I've ever written
            devConsole.AddCommand("savestate", delegate(object sender, CommandEventArgs e) {
                EntityIDCounterDebug = _entManager.EntityIDCounter;
                EntityCollection = _entManager.ActiveEntities;

                var mapSceneLUT =
                    JsonConvert.DeserializeObject<Dictionary<int, MapScene>>(
                        File.ReadAllText("data/map.lyr"));

                string currentState;
                int mapID = LyricalDebug.DebugConfig.LastMapSaveID;
                if (e.Args.Length > 0) {
                    if (e.Args[0] == "perm") {
                        mapSceneLUT[mapID] = this;
                        currentState = JsonConvert.SerializeObject(mapSceneLUT, Formatting.Indented);
                        File.WriteAllText("data/map.lyr", currentState);
                        LyricalDebug.Log("state saved to map.lyr");
                    } else {
                        mapSceneLUT[mapID] = this;
                        currentState = JsonConvert.SerializeObject(mapSceneLUT, Formatting.Indented);
                        File.WriteAllText("data/dev/scene_savestate_" + e.Args[0] + ".lyr",
                            currentState);
                        LyricalDebug.Log("state saved to scene_savestate_" + e.Args[0] + ".lyr");
                    }
                } else {
                    mapSceneLUT[mapID] = this;
                    currentState = JsonConvert.SerializeObject(mapSceneLUT, Formatting.Indented);
                    File.WriteAllText("data/dev/scene_savestate.lyr", currentState);
                    LyricalDebug.Log("state saved to scene_savestate.lyr");
                }
            }, "Save the state of the current mapScene.", "savestate [perm, <filename>]",
                "savestate - saves to file scene_savestate, can be quickloaded with 'loadstate'",
                "savestate perm - saves to file map.lyr, which is the default location to load mapScene data",
                "savestate <filename> - saves to file scene_savestate_<filename>.lyr");

            // fukken gross man
            devConsole.AddCommand("reassign", delegate(object sender, CommandEventArgs e) {
                if (e.Args[0] == "ahaeuhaeuhaeu") {
                    for (int i = 0; i < EntityCollection.Count; i++) {
                        _entManager.ActiveEntities[i].InstanceID = EntityCollection[i].InstanceID;
                    }
                    LyricalDebug.Log("Entity InstanceIDs re-assigned");
                } else {
                    LyricalDebug.Log("no, dont run reassign");
                }
            }, "dont run this");

            devConsole.AddCommand("ZENRYOKU_ZENKAI", delegate {
                _entManager.ClearAllActiveEntities();
                LyricalDebug.Log("ZENRYOKU ZENKAI!!!");
            }, "COMMENCING IMAGE TRAINING.", "Speed and power are essential in combat,",
                "but there are more important things.", "Do you know what it is?",
                devConsole.Prefix + "makenaitte kimochi, toka?", "Good answer, but what else.",
                devConsole.Prefix + "etto...", "Wisdom and tactics.",
                "How to fly and shoot, and the theory and practice of aerial combat,",
                "I will teach you these things.", "TRAINING READY?");
        }
#endif
    }
}