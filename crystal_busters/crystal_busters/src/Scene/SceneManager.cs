﻿#region Using Statements
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using VosSoft.Xna.GameConsole;

#endregion

namespace crystal_busters
{
    /* ----------------------------------------------------------------------------------
     * Description:
     * ● Runs as a Service
     * ● Manages ILyricalScene objects. Maintains a stack of Scenes, calls their update
     * loops, and designates a topmost scene (which all input is automatically routed
     * to).
     * ● This approach is based on the one provided by Microsoft here:　
     * http://xbox.create.msdn.com/en-US/education/catalog/sample/game_state_management
     * ---------------------------------------------------------------------------------- */

    public class SceneManager : ILyricalSceneManagerService
    {
        #region Default Constructor
        public SceneManager()
        {
            LyricalDebug.Assert(!_instantiated);
            _scenes = new List<Scene>();
            _idCounter = 0;
            _instantiated = true;

#if DEBUG
            AddDevConsoleCommands();
#endif
        }
        #endregion

        #region Fields
        private readonly List<Scene> _scenes; // Stack of Scenes
        private int _idCounter; // Scene unique ID
        private static bool _instantiated; // Instantiation assertion check
        #endregion

        #region Properties
        #endregion

        #region Update Loops
        public void Update(GameTime gameTime, InputHandler inputHandler)
        {
            // Create working copy of _scenes, this is done in case _scenes is modified during an Update
            var scenesToUpdate = new List<Scene>(_scenes);

            // Update all active scenes
            while (scenesToUpdate.Count > 0) {
                // Pop the topmost scene off the list
                Scene scn = scenesToUpdate[scenesToUpdate.Count - 1];
                scenesToUpdate.RemoveAt(scenesToUpdate.Count - 1);

                // Remove scenes marked for removal, or else update them
                if (scn.RemoveMeFromList) {
                    _scenes.Remove(scn);
                } else {
                    scn.Update(gameTime, inputHandler);
                }
            }
        }

        public void FixedUpdate()
        {
            foreach (Scene scn in _scenes) {
                scn.FixedUpdate();
            }
        }

        public virtual void Draw(GameTime gameTime, GraphicsDevice graphicsDevice,
            SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            Dictionary<string, SpriteFont> fontContent)
        {
            foreach (Scene scn in _scenes) {
                if (scn.SceneState == SceneTransitionState.Hidden) {
                    continue;
                }

                scn.Draw(gameTime, graphicsDevice, spriteBatch, textureContent, fontContent);
            }
        }
        #endregion

        #region Interface Functions
        public void AddScene(Scene scn)
        {
            scn.InstanceID = _idCounter;
            _scenes.Add(scn);
            _idCounter++;
        }
        #endregion

        #region Private Functions
        private void ClearAllActiveScenes()
        {
            _scenes.Clear();
        }
        #endregion

#if DEBUG
        private void AddDevConsoleCommands()
        {
            var devConsole = GameServices.GetService<GameConsole>();

            devConsole.AddCommand("get_scn", delegate(object obj, CommandEventArgs e) {
                var lyricalDebug = GameServices.GetService<ILyricalDebugService>();

                if (e.Args.Length > 0) {
                    int n;
                    bool result = int.TryParse(e.Args[0], out n);
                    if (result) {
                        List<Scene> l = _scenes.Where(scn => scn.InstanceID == n).ToList();
                        lyricalDebug.PrintScenes(l, true);
                    } else if (e.Args[0] == "properties" || e.Args[0] == "p") {
                        lyricalDebug.PrintScenes(_scenes, true);
                    } else {
                        LyricalDebug.Log("supply either a scn ID, or the argument `p`");
                    }
                } else {
                    lyricalDebug.PrintScenes(_scenes);
                }
            }, "Print scene information.", "get_scn [id] [p]");

            devConsole.AddCommand("map", delegate(object obj, CommandEventArgs com) {
                AddScene(new MapScene(int.Parse(com.Args[0])));
                LyricalDebug.Log($"Map ID {com.Args[0]} spawned.");
            }, "Spawn a mapscene, based on ID from map dict", "map <mapID>");

            devConsole.AddCommand("rm_scn", delegate(object sender, CommandEventArgs e) {
                foreach (Scene scn in _scenes) {
                    if (scn.InstanceID == int.Parse(e.Args[0])) {
                        _scenes.Remove(scn);
                    }
                }
            }, "Remove a scene, not safe", "rm <scene_instanceID>");

            devConsole.AddCommand("debug_tog_bg", delegate {
                LyricalDebug.DebugConfig.BackgroundVisible =
                    !LyricalDebug.DebugConfig.BackgroundVisible;
                LyricalDebug.Log("Show BG: " + LyricalDebug.DebugConfig.BackgroundVisible);
            });

            // needs refactor to remove runconfig
            devConsole.AddCommand("debug_tog_draw", delegate(object obj, CommandEventArgs com) {
                if (com.Args.Length > 0) {
                    if (com.Args[0] == "all") {
                        switch (com.Args[1]) {
                            case "on":
                                LyricalDebug.DebugConfig.HurtboxesVisible = true;
                                LyricalDebug.DebugConfig.MainGridVisible = true;
                                LyricalDebug.DebugConfig.MainGridDividerVisible = true;
                                LyricalDebug.DebugConfig.PlayerGridVisible = true;
                                LyricalDebug.DebugConfig.PlayerGridDividerVisible = true;
                                LyricalDebug.Log("Enable drawing of all debug borders");
                                break;
                            case "off":
                                LyricalDebug.DebugConfig.HurtboxesVisible = false;
                                LyricalDebug.DebugConfig.MainGridVisible = false;
                                LyricalDebug.DebugConfig.MainGridDividerVisible = false;
                                LyricalDebug.DebugConfig.PlayerGridVisible = false;
                                LyricalDebug.DebugConfig.PlayerGridDividerVisible = false;
                                LyricalDebug.Log("Disable drawing of all debug borders");
                                break;
                            default:
                                devConsole.Log("supply `on` or `off` when using all.");
                                break;
                        }
                    } else {
                        foreach (string arg in com.Args) {
                            switch (arg) {
                                case "hurtbox":
                                    LyricalDebug.DebugConfig.HurtboxesVisible =
                                        !LyricalDebug.DebugConfig.HurtboxesVisible;
                                    LyricalDebug.Log("Show hurtboxes: " +
                                                     LyricalDebug.DebugConfig.HurtboxesVisible);
                                    break;
                                case "maingrid":
                                    LyricalDebug.DebugConfig.MainGridVisible =
                                        !LyricalDebug.DebugConfig.MainGridVisible;
                                    LyricalDebug.Log("Show main grid: " +
                                                     LyricalDebug.DebugConfig.MainGridVisible);
                                    break;
                                case "maingriddivider":
                                    LyricalDebug.DebugConfig.MainGridDividerVisible =
                                        !LyricalDebug.DebugConfig.MainGridDividerVisible;
                                    LyricalDebug.Log("Show main grid divider: " +
                                                     LyricalDebug.DebugConfig.MainGridDividerVisible);
                                    break;
                                case "playergrid":
                                    LyricalDebug.DebugConfig.PlayerGridVisible =
                                        !LyricalDebug.DebugConfig.PlayerGridVisible;
                                    LyricalDebug.Log("Show player grid: " +
                                                     LyricalDebug.DebugConfig.PlayerGridVisible);
                                    break;
                                case "playergriddivider":
                                    LyricalDebug.DebugConfig.PlayerGridDividerVisible =
                                        !LyricalDebug.DebugConfig.PlayerGridDividerVisible;
                                    LyricalDebug.Log("Show player grid divider: " +
                                                     LyricalDebug.DebugConfig
                                                         .PlayerGridDividerVisible);
                                    break;
                                default:
                                    devConsole.Log($"Argurment {arg} is invalid", 1);
                                    devConsole.ExecManual("debug_tog_draw");
                                    break;
                            }
                        }
                    }
                } else {
                    LyricalDebug.DebugConfig.HurtboxesVisible =
                        !LyricalDebug.DebugConfig.HurtboxesVisible;
                    LyricalDebug.DebugConfig.MainGridVisible =
                        !LyricalDebug.DebugConfig.MainGridVisible;
                    LyricalDebug.DebugConfig.MainGridDividerVisible =
                        !LyricalDebug.DebugConfig.MainGridDividerVisible;
                    LyricalDebug.DebugConfig.PlayerGridVisible =
                        !LyricalDebug.DebugConfig.PlayerGridVisible;
                    LyricalDebug.DebugConfig.PlayerGridDividerVisible =
                        !LyricalDebug.DebugConfig.PlayerGridDividerVisible;
                }
            }, "Toggle drawing of debug borders",
                "<all [on,off]> <hurtbox> <maingrid> <maingriddivider> <playergrid> <playergriddivider>");

            devConsole.AddCommand("loadstate", delegate(object sender, CommandEventArgs e) {
                string savefile;

                if (e.Args.Length > 0) {
                    savefile = "data/dev/scene_savestate_" + e.Args[0] + ".lyr";
                    if (System.IO.File.Exists(savefile)) {
                        LyricalDebug.DebugConfig.MapDataLocation = savefile;
                    } else {
                        LyricalDebug.Log(savefile + " doesn't exist.");
                        return;
                    }
                } else {
                    savefile = "data/dev/scene_savestate.lyr";
                }

                devConsole.Execute("IKUYO_RAISING_HAATO", true);
                LyricalDebug.DebugConfig.MapDataLocation = savefile;

                devConsole.Execute("map " + LyricalDebug.DebugConfig.LastMapSaveID);
                devConsole.Execute("reassign ahaeuhaeuhaeu", false);
            }, "Loads previously saved state.",
                "If no arguments are supplied, it loads based on the ID of the last saved mapScene during this runtime. If that's nothing, it defaults to 1.",
                "IF you use savestate and loadstate with no args on the same run, InstanceIDs should be retained.",
                "Otherwise all bets are off on instanceIDs.", "loadstate [statename] [mapID]");

            devConsole.AddCommand("IKUYO_RAISING_HAATO", delegate {
                LyricalDebug.Log("Stand by? ready.");
                devConsole.Execute("ZENRYOKU_ZENKAI", false);
                LyricalDebug.Log("Cannon mode.");
                ClearAllActiveScenes();
                LyricalDebug.Log("SUTAAAAAAAAARAIIITOOOOOOOOOOOO " +
                                 "BUREIIIIIIIIIIIIKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA----------------------!!!!!!");
            }, "RAISING HAATO, ONEGAI");
        }
#endif
    }
}