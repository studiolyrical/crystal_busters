﻿#region Using Statements
using Microsoft.Xna.Framework;

#endregion

namespace crystal_busters
{
    public class Camera
    {
        #region Default Constructor
        public Camera()
        {
            _viewPos = new Vector2();
            _scale = 0;
            _zoom = 0;
            Position = new Vector2();
            Rotation = 0;
            BoundaryMin = new Vector2();
            BoundaryMax = new Vector2();
        }
        #endregion

        #region Parameterized Constructors
        public Camera(Vector2 viewPos, float scale)
            : this()
        {
            _viewPos = viewPos;
            _scale = scale;
        }
        #endregion

        #region Fields
        private Vector2 _viewPos; // Screen position coords
        private readonly float _scale; // Camera resolution scaling
        private float _zoom; // Camera Zoom level
        #endregion

        #region Properties
        public Vector2 Position { get; set; } // World position cords
        public float Rotation { get; set; }
        public Vector2 BoundaryMin { get; set; }
        public Vector2 BoundaryMax { get; set; }

        public float Zoom {
            get { return _zoom; }
            set {
                _zoom = value;
                // prevent negative zoom
                if (_zoom < 0.00f) {
                    _zoom = 0.00f;
                }
            }
        }
        #endregion

        #region Public Functions
        /* ----------------------------------------------------------------------------------
         * ● This function returns a matrix that's used to translate between world space and
         * screen space. 
         * ● This is primarily done to allow us to use arbitrary values for world space
         * coordinates, and then translate that to screenspace using any resolution properly.
         * ● A bonus comes in the ability to rotate and zoom the camera as well.
         * ---------------------------------------------------------------------------------- */

        public Matrix GetViewMatrix(float paraX = 1.0f, float paraY = 1.0f)
        {
            Matrix mat = Matrix.CreateTranslation(-Position.X * paraX, -Position.Y * paraY, 0.0f) *
                         Matrix.CreateRotationZ(Rotation) * Matrix.CreateScale(_scale * _zoom) *
                         Matrix.CreateTranslation(_viewPos.X, _viewPos.Y, 0.0f);

            return mat;
        }

        // This function prevents the Camera's position from going outside the specified min/max boundaries.
        public void ClampCameraBoundary()
        {
            var cameraClampValue =
                new Vector2(MathHelper.Clamp(Position.X, BoundaryMin.X, BoundaryMax.X),
                    MathHelper.Clamp(Position.Y, BoundaryMin.Y, BoundaryMax.Y));

            Position = cameraClampValue;
        }

        public void LookAt(Vector2 pos)
        {
            Position = pos;
        }
        #endregion
    }
}