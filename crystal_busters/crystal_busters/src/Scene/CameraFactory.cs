﻿#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace crystal_busters
{
    public class CameraFactory
    {
        #region Default Constructor
        public CameraFactory() {}
        #endregion

        #region Public Functions
        public Camera CreateCamera(Vector2 position, float zoom, float rotation, Vector2 boundMin,
            Vector2 boundMax)
        {
            const int RESOLUTION_SCALING = 1500;

            // Setup initial values for new camera
            Viewport viewport =
                GameServices.GetService<GraphicsDeviceManager>().GraphicsDevice.Viewport;
            var viewPos = new Vector2(viewport.Width * 0.5f, viewport.Height * 0.5f);
            var scale = (float)viewport.Width / RESOLUTION_SCALING;

            var newCamera = new Camera(viewPos, scale) {
                Position = position, Zoom = zoom, Rotation = rotation, BoundaryMin = boundMin,
                BoundaryMax = boundMax
            };

            return newCamera;
        }
        #endregion
    }
}