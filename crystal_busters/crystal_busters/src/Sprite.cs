namespace crystal_busters
{
    // A sprite holds information that helps define how a Texture2D should be drawn
    public class Sprite
    {
        #region Default Constructor
        public Sprite()
        {
            Name = "blank";
            Columns = 1;
            Rows = 1;
            FrameRange = 1;
            CurrentFrame = 0;
            SampleRate = 0;
            Accumulator = 0;
        }
        #endregion

        #region Copy Constructor
        public Sprite(Sprite bSprite)
            : this()
        {
            Name = bSprite.Name;
            Columns = bSprite.Columns;
            Rows = bSprite.Rows;
            FrameRange = bSprite.FrameRange;
            CurrentFrame = bSprite.CurrentFrame;
            SampleRate = bSprite.SampleRate;
            Accumulator = bSprite.Accumulator;
        }
        #endregion

        #region Properties
        public string Name { get; set; }
        public int Columns { get; set; } // Total columns in the Atlas
        public int Rows { get; set; } // Total rows in the Atlas
        public int FrameRange { get; set; } // Total number of animation frames in the TextureAtlas
        public int CurrentFrame { get; set; } // The frame that the animator is currently on
        public int SampleRate { get; set; } // Framerate of the animation
        public int Accumulator { get; set; } // This should normally be set by DrawSystem()
        #endregion
    }
}