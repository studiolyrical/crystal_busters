﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Squid;

namespace crystal_busters
{
    public class MapSceneCanvas : Desktop
    {
        #region Default Constructor
        public MapSceneCanvas()
        {
            Skin = new SquidSkin();
            CursorSet = new VanillaCursorSet();
            TooltipControl = new SimpleTooltip();

            Info = new InfoWindow();
            Info.Show(this);

            UnitActions = new UnitActionMenu();
            UnitActions.Show(this);
        }
        #endregion

        #region Properties
        public InfoWindow Info { get; set; }
        public UnitActionMenu UnitActions { get;set; }
        #endregion

        #region Public Functions
        protected override void OnUpdate()
        {
        }
        #endregion

    }
}
