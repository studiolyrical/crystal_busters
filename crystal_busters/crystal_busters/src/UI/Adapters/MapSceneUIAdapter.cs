﻿#region Using Statements
using Microsoft.Xna.Framework;
using Point = Squid.Point;
#endregion

// intermediate layer between gameplay and UI
// might seem redundant with Canvas, but this is library change safe.

namespace crystal_busters
{
    public class MapSceneUIAdapter
    {
        #region Default Constructor
        public MapSceneUIAdapter()
        {
            _mapSceneUI = new MapSceneCanvas {ShowCursor = true};
            _prevEntity = new Entity();
        }
        #endregion

        #region Fields
        private readonly MapSceneCanvas _mapSceneUI;
        private Entity _prevEntity;
        #endregion

        #region Public Functions
        public void Update(Entity hoverActor, Entity selectedUnit)
        {
            var gdm = GameServices.GetService<GraphicsDeviceManager>();
            _mapSceneUI.Size = new Point(gdm.GraphicsDevice.Viewport.Width,
                gdm.GraphicsDevice.Viewport.Height);
            _mapSceneUI.Info.HoverActor = hoverActor;

            // set the UnitActionMenu ActionUnit if selectedUnit has changed
            // This is important because the buttons in UnitMenu are setup when
            // the setter on ActionUnit is called.
            if (_prevEntity != selectedUnit) {
                if (_mapSceneUI.UnitActions.TargetingMode) {
                    _mapSceneUI.UnitActions.TargetUnit = selectedUnit;
                    _mapSceneUI.UnitActions.TargetingMode = false;
                } else {
                    _mapSceneUI.UnitActions.ActionUnit = selectedUnit;
                }
                _prevEntity = selectedUnit;
            }

            _mapSceneUI.Update();
        }

        public void Draw()
        {
            _mapSceneUI.Draw();
        }
        #endregion
    }
}
