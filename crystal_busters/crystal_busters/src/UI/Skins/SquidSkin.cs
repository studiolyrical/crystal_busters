﻿#region Using Statements
using Squid;
#endregion

namespace crystal_busters
{
    public class SquidSkin : Skin
    {
        public SquidSkin()
        {
            var baseStyle = new ControlStyle {
                Tiling = TextureMode.Grid, Grid = new Margin(3), Texture = "skin_squid/button_hot.dds",
                Default = {Texture = "skin_squid/button_default.dds"}, Pressed = {Texture = "skin_squid/button_down.dds"},
                SelectedPressed = {Texture = "skin_squid/button_down.dds"},
                Focused = {Texture = "skin_squid/button_down.dds"},
                SelectedFocused = {Texture = "skin_squid/button_down.dds"},
                Selected = {Texture = "skin_squid/button_down.dds"},
                SelectedHot = {Texture = "skin_squid/button_down.dds"}
            };

            var itemStyle = new ControlStyle(baseStyle)
            {TextPadding = new Margin(8, 0, 8, 0), TextAlign = Alignment.MiddleLeft};

            var buttonStyle = new ControlStyle(baseStyle)
            {TextPadding = new Margin(0), TextAlign = Alignment.MiddleCenter};

            var tooltipStyle = new ControlStyle(buttonStyle)
            {TextPadding = new Margin(8), TextAlign = Alignment.TopLeft};

            var inputStyle = new ControlStyle {
                Texture = "skin_squid/input_default.dds", Hot = {Texture = "skin_squid/input_focused.dds"},
                Focused = {Texture = "skin_squid/input_focused.dds"}, TextPadding = new Margin(8),
                Tiling = TextureMode.Grid
            };
            inputStyle.Focused.Tint = ColorInt.ARGB(1f, 1f, 0, 0);
            inputStyle.Grid = new Margin(3);

            var windowStyle = new ControlStyle
            {Tiling = TextureMode.Grid, Grid = new Margin(9), Texture = "skin_squid/window.dds"};

            var frameStyle = new ControlStyle {
                Tiling = TextureMode.Grid, Grid = new Margin(4), Texture = "skin_squid/frame.dds",
                TextPadding = new Margin(8)
            };

            var vscrollTrackStyle = new ControlStyle
            {Tiling = TextureMode.Grid, Grid = new Margin(3), Texture = "skin_squid/vscroll_track.dds"};

            var vscrollButtonStyle = new ControlStyle {
                Tiling = TextureMode.Grid, Grid = new Margin(3), Texture = "skin_squid/vscroll_button.dds",
                Hot = {Texture = "skin_squid/vscroll_button_hot.dds"},
                Pressed = {Texture = "skin_squid/vscroll_button_down.dds"}
            };

            var vscrollUp = new ControlStyle {
                Default = {Texture = "skin_squid/vscrollUp_default.dds"}, Hot = {Texture = "skin_squid/vscrollUp_hot.dds"},
                Pressed = {Texture = "skin_squid/vscrollUp_down.dds"},
                Focused = {Texture = "skin_squid/vscrollUp_hot.dds"}
            };

            var hscrollTrackStyle = new ControlStyle
            {Tiling = TextureMode.Grid, Grid = new Margin(3), Texture = "skin_squid/hscroll_track.dds"};

            var hscrollButtonStyle = new ControlStyle {
                Tiling = TextureMode.Grid, Grid = new Margin(3), Texture = "skin_squid/hscroll_button.dds",
                Hot = {Texture = "skin_squid/hscroll_button_hot.dds"},
                Pressed = {Texture = "skin_squid/hscroll_button_down.dds"}
            };

            var hscrollUp = new ControlStyle {
                Default = {Texture = "skin_squid/hscrollUp_default.dds"}, Hot = {Texture = "skin_squid/hscrollUp_hot.dds"},
                Pressed = {Texture = "skin_squid/hscrollUp_down.dds"},
                Focused = {Texture = "skin_squid/hscrollUp_hot.dds"}
            };

            var checkButtonStyle = new ControlStyle {
                Default = {Texture = "skin_squid/checkbox_default.dds"}, Hot = {Texture = "skin_squid/checkbox_hot.dds"},
                Pressed = {Texture = "skin_squid/checkbox_down.dds"},
                Checked = {Texture = "skin_squid/checkbox_checked.dds"},
                CheckedFocused = {Texture = "skin_squid/checkbox_checked_hot.dds"},
                CheckedHot = {Texture = "skin_squid/checkbox_checked_hot.dds"},
                CheckedPressed = {Texture = "skin_squid/checkbox_down.dds"}
            };

            var comboLabelStyle = new ControlStyle {
                TextPadding = new Margin(10, 0, 0, 0), Default = {Texture = "skin_squid/combo_default.dds"},
                Hot = {Texture = "skin_squid/combo_hot.dds"}, Pressed = {Texture = "skin_squid/combo_down.dds"},
                Focused = {Texture = "skin_squid/combo_hot.dds"}, Tiling = TextureMode.Grid,
                Grid = new Margin(3, 0, 0, 0)
            };

            var comboButtonStyle = new ControlStyle {
                Default = {Texture = "skin_squid/combo_button_default.dds"},
                Hot = {Texture = "skin_squid/combo_button_hot.dds"},
                Pressed = {Texture = "skin_squid/combo_button_down.dds"},
                Focused = {Texture = "skin_squid/combo_button_hot.dds"}
            };

            var multilineStyle = new ControlStyle
            {TextAlign = Alignment.TopLeft, TextPadding = new Margin(0)};

            var labelStyle = new ControlStyle {
                TextPadding = new Margin(8, 0, 8, 0), TextAlign = Alignment.MiddleLeft,
                TextColor = ColorInt.ARGB(1, .8f, .8f, .8f), BackColor = 0,
                Hot = {BackColor = ColorInt.ARGB(.125f, 1, 1, 1)}
            };

            Add("item", itemStyle);
            Add("textbox", inputStyle);
            Add("button", buttonStyle);
            Add("window", windowStyle);
            Add("frame", frameStyle);
            Add("checkBox", checkButtonStyle);
            Add("comboLabel", comboLabelStyle);
            Add("comboButton", comboButtonStyle);
            Add("vscrollTrack", vscrollTrackStyle);
            Add("vscrollButton", vscrollButtonStyle);
            Add("vscrollUp", vscrollUp);
            Add("hscrollTrack", hscrollTrackStyle);
            Add("hscrollButton", hscrollButtonStyle);
            Add("hscrollUp", hscrollUp);
            Add("multiline", multilineStyle);
            Add("tooltip", tooltipStyle);
            Add("label", labelStyle);
        }
    }
}
