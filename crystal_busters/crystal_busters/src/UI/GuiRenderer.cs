﻿#region Using Statements
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Squid;
#endregion

namespace crystal_busters
{
    public class GUIRenderer : ISquidRenderer
    {
        #region Default Constructor
        public GUIRenderer()
        {
            _game = null;
            _spriteBatch = null;
            _whiteTexture = null;
            _sampler = null;
            _rasterizer = null;
            _fontByID = new Dictionary<int, SpriteFont>();
            _fontIDString = _fontIDString = new Dictionary<string, int>();
            _textureByID = _textureByID = new Dictionary<int, Texture2D>();
            _textureIDString = new Dictionary<string, int>();
        }
        #endregion

        #region Parameterized Constructors
        public GUIRenderer(Game game)
            : this()
        {
            _game = game;
            _spriteBatch = new SpriteBatch(game.GraphicsDevice);
            _whiteTexture = new Texture2D(_game.GraphicsDevice, 1, 1);
            _whiteTexture.SetData(new[] { new Color(255, 255, 255, 255) });
            _rasterizer = new RasterizerState { ScissorTestEnable = true };
            _sampler = new SamplerState { Filter = TextureFilter.Anisotropic };
        }
        #endregion

        #region Fields
        private readonly Game _game;
        private readonly SpriteBatch _spriteBatch;
        private readonly Texture2D _whiteTexture;
        private readonly SamplerState _sampler;
        private readonly RasterizerState _rasterizer;

        private readonly Dictionary<int, SpriteFont> _fontByID;
        private readonly Dictionary<string, int> _fontIDString;
        private readonly Dictionary<int, Texture2D> _textureByID;
        private readonly Dictionary<string, int> _textureIDString;

        private int _tempId;
        private SpriteFont _tempFont;
        private Texture2D _tempTex;

        private const string DEFAULT_FONT = "font/devtext";
        #endregion

        #region Public Functions
        public int GetTexture(string name)
        {
            if (_textureIDString.TryGetValue(name, out _tempId)) {
                return _tempId;
            }

            var texture = _game.Content.Load<Texture2D>(Path.ChangeExtension("gui/" + name, null));
            int index = _textureIDString.Count;
            _textureIDString.Add(name, index);
            _textureByID.Add(index, texture);
            return index;
        }

        public int GetFont(string name)
        {
            if (name == "default") {
                name = DEFAULT_FONT;
            }

            if (_fontIDString.TryGetValue(name, out _tempId)) {
                return _tempId;
            }

            var font = _game.Content.Load<SpriteFont>(name);
            int index = _fontIDString.Count;

            _fontIDString.Add(name, index);
            _fontByID.Add(index, font);

            return index;
        }

        public Squid.Point GetTextSize(string text, int font)
        {
            if (string.IsNullOrEmpty(text)) {
                return Squid.Point.Zero;
            }

            if (!_fontByID.TryGetValue(font, out _tempFont)) {
                return Squid.Point.Zero;
            }

            Vector2 size = _tempFont.MeasureString(text);
            return new Squid.Point((int)size.X, (int)size.Y);
        }

        public Squid.Point GetTextureSize(int texture)
        {
            if (_textureByID.TryGetValue(texture, out _tempTex)) {
                return new Squid.Point(_tempTex.Width, _tempTex.Height);
            }

            return Squid.Point.Zero;
        }

        public void DrawBox(int x, int y, int w, int h, int color)
        {
            _spriteBatch.Draw(_whiteTexture, new Microsoft.Xna.Framework.Rectangle(x, y, w, h),
                LyricalHelper.ColorFromInt32(color));
        }

        public void DrawText(string text, int x, int y, int font, int color)
        {
            if (_fontByID.TryGetValue(font, out _tempFont)) {
                _spriteBatch.DrawString(_tempFont, text, new Vector2(x, y),
                    LyricalHelper.ColorFromInt32(color));
            }
        }

        public void DrawTexture(int texture, int x, int y, int w, int h, Squid.Rectangle rect, int color)
        {
            if (_textureByID.TryGetValue(texture, out _tempTex)) {
                _spriteBatch.Draw(_tempTex, new Microsoft.Xna.Framework.Rectangle(x, y, w, h),
                    new Microsoft.Xna.Framework.Rectangle(rect.Left, rect.Top, rect.Width,
                        rect.Height), LyricalHelper.ColorFromInt32(color));
            }
        }

        public void Scissor(int x, int y, int w, int h)
        {
            _game.GraphicsDevice.ScissorRectangle =
                new Microsoft.Xna.Framework.Rectangle(x < 0 ? 0 : x, y < 0 ? 0 : y, w, h);
        }

        public void StartBatch()
        {
            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, _sampler, null,
                _rasterizer);
        }

        public void EndBatch(bool final)
        {
            _spriteBatch.End();
        }

        public void Dispose() { }
        #endregion
    }
}