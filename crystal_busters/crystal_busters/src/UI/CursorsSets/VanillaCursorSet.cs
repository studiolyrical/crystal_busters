﻿#region Using Statements
using Squid;
#endregion

namespace crystal_busters
{
    public class VanillaCursorSet : CursorCollection
    {
        public VanillaCursorSet()
        {
            var cursorSize = new Point(32, 32);
            Point halfSize = cursorSize / 2;

            Add(CursorNames.Default,
                new Cursor {Texture = "vanilla_cursor\\Arrow.png", Size = cursorSize, HotSpot = Point.Zero});
            Add(CursorNames.Link,
                new Cursor {Texture = "vanilla_cursor\\Link.png", Size = cursorSize, HotSpot = Point.Zero});
            Add(CursorNames.Move,
                new Cursor {Texture = "vanilla_cursor\\Move.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.Select,
                new Cursor {Texture = "vanilla_cursor\\Select.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.SizeNS,
                new Cursor {Texture = "vanilla_cursor\\SizeNS.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.SizeWE,
                new Cursor {Texture = "vanilla_cursor\\SizeWE.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.HSplit,
                new Cursor {Texture = "vanilla_cursor\\SizeNS.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.VSplit,
                new Cursor {Texture = "vanilla_cursor\\SizeWE.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.SizeNESW,
                new Cursor
                {Texture = "vanilla_cursor\\SizeNESW.png", Size = cursorSize, HotSpot = halfSize});
            Add(CursorNames.SizeNWSE,
                new Cursor
                {Texture = "vanilla_cursor\\SizeNWSE.png", Size = cursorSize, HotSpot = halfSize});
        }
    }
}
