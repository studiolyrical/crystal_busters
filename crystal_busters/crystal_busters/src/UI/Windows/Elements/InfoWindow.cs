﻿#region Using Statements
using Squid;
#endregion

namespace crystal_busters
{
    public class InfoWindow : SquidWindow
    {
        #region Default Constructor
        public InfoWindow()
        {
            Size = new Point(200, 200);
            Position = new Point(300, 300);
            Titlebar.Text = "Information";

            _classLabel = new Label {Position = new Point(20, 50), Size = new Point(500, 10)};
            Controls.Add(_classLabel);
        }
        #endregion

        #region Properties
        public Entity HoverActor { get; set; }
        #endregion

        #region Fields
        private readonly Label _classLabel;
        #endregion

        #region Inherited Functions
        protected override void OnUpdate()
        {
            if (HoverActor.Name != "") {
                Titlebar.Text = HoverActor.Name;
                _classLabel.Text = "Instance ID: " + HoverActor.InstanceID;
            } else {
                Titlebar.Text = "Information";
                _classLabel.Text = "";
            }
            base.OnUpdate();
        }
        #endregion
    }
}
