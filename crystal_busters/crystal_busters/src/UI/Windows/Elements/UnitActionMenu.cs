﻿#region Using Statements
using System;
using Squid;
#endregion

namespace crystal_busters
{
    public class UnitActionMenu : SquidWindow
    {
        #region Default Constructor
        public UnitActionMenu()
        {
            _actionUnit = new Entity();
            TargetUnit = new Entity();

            Size = new Point(500, 300);
            Position = new Point(300, 300);
            Titlebar.Text = "Select unit";
            Resizable = true;

            SetButtons();
        }
        #endregion

        #region Fields
        private Entity _actionUnit;
        #endregion

        #region Properties
        public Entity ActionUnit {
            get { return _actionUnit; }
            set {
                _actionUnit = value;
                SetButtons();
            }
        }

        public Entity TargetUnit { get; set; }
        public bool TargetingMode { get; set; }
        #endregion

        #region Event Handlers
        private void attackButton_OnMouseClick(Control sender, MouseEventArgs args)
        {
            TargetingMode = true;
        }
        #endregion

        #region Public Functions
        public void SetButtons()
        {
            var attackButton = new Button();
            if (ActionUnit != null && ActionUnit.Name != "") {
                Titlebar.Text = ActionUnit.Name;
                attackButton = new Button {
                    Size = new Point(157, 35), Position = new Point(20, 50), Text = "Attack",
                    Style = "button", Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
                    Cursor = CursorNames.Link
                };
                attackButton.MouseClick += attackButton_OnMouseClick;
                Controls.Add(attackButton);
            } else {
                Controls.Remove(attackButton);
            }

        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
        }
        #endregion
    }
}
