﻿#region Using Statements
using crystal_busters.GUIActions;
using Squid;
#endregion

namespace crystal_busters
{
    public class SquidTitleBar : Label
    {
        #region Default Constructor
        public SquidTitleBar()
        {
            Button = new Button {
                Size = new Point(30, 30), Style = "button", Tooltip = "Close Window",
                Dock = DockStyle.Right, Margin = new Margin(0, 8, 8, 8)
            };
            Elements.Add(Button);
        }
        #endregion

        #region Properties
        public Button Button { get; private set; }
        #endregion
    }

    public class SquidWindow : Window
    {
        #region Default Constructor
        public SquidWindow()
        {
            AllowDragOut = true;
            Padding = new Margin(4);
            Titlebar = new SquidTitleBar {
                Dock = DockStyle.Top, Size = new Point(122, 35), Cursor = CursorNames.Move,
                Style = "frame", Margin = new Margin(0, 0, 0, -1), TextAlign = Alignment.MiddleLeft,
                BBCodeEnabled = true
            };
            Titlebar.MouseDown += delegate { StartDrag(); };
            Titlebar.MouseUp += delegate { StopDrag(); };
            Titlebar.Button.MouseClick += Button_OnMouseClick;
            Controls.Add(Titlebar);
        }
        #endregion

        #region Properties
        public SquidTitleBar Titlebar { get; private set; }
        #endregion

        #region EventHandlers
        private void Button_OnMouseClick(Control sender, MouseEventArgs args)
        {
            Actions.Add(new Fade(0, 250)).Finished += window => { Close(); };
        }
        #endregion

        #region Public Functions
        public override void Show(Desktop target)
        {
            Opacity = 0;
            base.Show(target);
            Actions.Add(new Fade(1, 250));
        }
        #endregion
    }
}
