﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace crystal_busters
{
    public class EventQueueUI
    {
        #region Default Constructor
        public EventQueueUI()
        {
            EventList = new List<EventUI>();
        }
        #endregion

        #region Properties
        public List<EventUI> EventList;
        #endregion

        #region Public Functions
        public void AddEvent()
        {
            
        }

        public void GetEvents()
        {
            
        }
        #endregion
    }
}
