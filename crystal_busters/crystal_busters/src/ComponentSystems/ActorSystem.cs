﻿#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace crystal_busters
{
    public class ActorSystem : ILyricalComponentSystem
    {
        #region Default Constructor
        #endregion

        #region Fields
        #endregion

        #region Properties
        public Entity HoverActor { get; set; }
        public Entity SelectedUnit { get; set; }
        #endregion

        #region Update Loops
        public void Update(ILyricalInputHandlerService inputHandler, List<Entity> actors, Camera cam)
        {
            HoverActor = new Entity();

            Point mousePoint = LyricalHelper.Vector2ToPoint(inputHandler.GetMousePosition(cam));
            foreach (Entity actor in actors) {
                // Match the entity hurtbox with the entity's position
                actor.ActorComponent.Hurtbox =
                    new Rectangle(
                        (int) actor.PositionComponent.Position.X + -actor.ActorComponent.Hurtbox.Width / 2 +
                        (int) actor.ActorComponent.HurtboxOffset.X,
                        (int) actor.PositionComponent.Position.Y + -actor.ActorComponent.Hurtbox.Height / 2 +
                        (int) actor.ActorComponent.HurtboxOffset.Y,
                        actor.ActorComponent.Hurtbox.Width, actor.ActorComponent.Hurtbox.Height);

                if (actor.DrawComponent != null) {
                    if (actor.ActorComponent.Hurtbox.Contains(mousePoint)) {
                        HoverActor = actor;
                        if (inputHandler.PressOnceMouse("LeftButton")) {
                            SelectedUnit = actor;
                        }
                    } else {
                        actor.DrawComponent.Flash = false;
                    }
                }
            }
#if DEBUG
            var lyricalDebug = GameServices.GetService<ILyricalDebugService>();
            lyricalDebug.DebugUpdate(inputHandler, actors, cam);
#endif
        }

        public void FixedUpdate()
        {
            // TODO: needs gametime refactor
            if (HoverActor.DrawComponent != null) {
                HoverActor.DrawComponent.Flash = !HoverActor.DrawComponent.Flash;
            }
        }
        #endregion
    }
}