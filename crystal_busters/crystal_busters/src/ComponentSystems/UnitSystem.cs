﻿#region Using Statements

#endregion

namespace crystal_busters
{
    public class UnitSystem : ILyricalComponentSystem
    {
        #region Default Constructor
        public UnitSystem() {}
        #endregion

        #region Fields
        public Entity SelectedUnit;
        #endregion

        #region EventHandlers
        #endregion

        #region Public Functions
        public void Update() {}
        #endregion

        #region Private Functions
        #endregion
    }
}