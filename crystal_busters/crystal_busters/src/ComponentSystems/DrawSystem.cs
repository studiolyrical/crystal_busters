﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

/* ----------------------------------------------------------------------------------
 * Description:
 * ● Puts stuff on screen, with parallax scrolling
 *     ● Note that parallax scrolling logic itself is handled in the camera matrix, 
 *       but this class handles multiple layer functionality.
 * ● Doubles as my AnimationSystem since logic for animating sprite atlases is simple
 * ---------------------------------------------------------------------------------- */

namespace crystal_busters
{
    public class DrawSystem : ILyricalComponentSystem
    {
        #region Default Constructor
        public DrawSystem()
        {
            _parallaxLayersArray = new List<Entity>[0];
        }
        #endregion

        #region Fields
        private List<Entity>[] _parallaxLayersArray;
        private const int DELTA_TIME = 100; // TODO: FrameStep() needs gameTime refactor
        #endregion

        #region Update Loops
        public void Update(List<Entity> entities, int paralayerCount)
        {
            SetParallaxLayerArrayLength(paralayerCount);

            foreach (Entity ent in entities) {
                AddEntitiesToParallaxLayers(ent);
            }
        }

        public void FixedUpdate(List<Entity> entities)
        {
            foreach (Entity ent in entities) {
                FrameStep(ent);

                // TODO: sprite flashing, needs gameTime refactor and moved to Update()
                if (ent.DrawComponent.Flash) {
                    if (ent.DrawComponent.CurrentSprite.CurrentFrame % 2 == 0) {
                        ent.DrawComponent.TintColor = new Color(150, 150, 150, ent.DrawComponent.TintColor.A);
                    }
                } else {
                    ent.DrawComponent.TintColor = new Color(255, 255, 255, ent.DrawComponent.TintColor.A);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            Camera sceneCamera, List<Vector2> paralayers)
        {
            if (_parallaxLayersArray != null) {
                // Start a spriteBatch for each ParallaxLayer
                for (int i = _parallaxLayersArray.Length - 1; i > -1; i--) {
                    // Begin spritebatch for whole Paralayer
                    spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend,
                        SamplerState.PointClamp, null, null, null,
                        sceneCamera.GetViewMatrix(paralayers[i].X, paralayers[i].Y));

                    foreach (Entity ent in _parallaxLayersArray[i]) {
                        Texture2D tex;

                        try {
                            tex = textureContent[ent.DrawComponent.CurrentSprite.Name];
                        } catch (Exception) {
                            LyricalDebug.ExceptionMessage(
                                "Texture name doesn't exist in our texture map\n" + "Entity name: " +
                                ent.Name + " // Texture name: " +
                                ent.DrawComponent.CurrentSprite.Name);
                            Debugger.Break();
                            throw;
                        }

                        ent.DrawComponent.CurrentSprite = ent.DrawComponent.NextSprite;

                        Rectangle sourceRectangle;
                        Rectangle destinationRectangle;

                        // Try to setup the data for drawing the entity with animation, based on whatever FrameStep() has set
                        // Mostly lifted from rbwhitaker: http://rbwhitaker.wikidot.com/texture-atlases-2
                        try {
                            int width = tex.Width / ent.DrawComponent.CurrentSprite.Columns;
                            int height = tex.Height / ent.DrawComponent.CurrentSprite.Rows;
                            int row =
                                (int)
                                    (ent.DrawComponent.CurrentSprite.CurrentFrame /
                                     (float) ent.DrawComponent.CurrentSprite.Columns);
                            int column = ent.DrawComponent.CurrentSprite.CurrentFrame %
                                         ent.DrawComponent.CurrentSprite.Columns;

                            sourceRectangle = new Rectangle(width * column, height * row, width,
                                height);
                            destinationRectangle =
                                new Rectangle((int) ent.PositionComponent.Position.X,
                                    (int) ent.PositionComponent.Position.Y,
                                    ent.DrawComponent.Scale.X * width,
                                    ent.DrawComponent.Scale.Y * height);

                            ent.DrawComponent.Origin = new Vector2(width * 0.5f, height * 0.5f);
                        } catch (Exception) {
                            LyricalDebug.ExceptionMessage(
                                "Entity can't be drawn, did you forget to assign values to the " +
                                "DrawComponent fields?\nEntity Info:\n" + ent.Name + "(ID:" +
                                ent.InstanceID + ")\nSprite == " +
                                ent.DrawComponent.CurrentSprite.Name + "\nColumns == " +
                                ent.DrawComponent.CurrentSprite.Columns + " // Rows == " +
                                ent.DrawComponent.CurrentSprite.Rows + " // TotalFrames == " +
                                ent.DrawComponent.CurrentSprite.FrameRange);
                            Debugger.Break();
                            throw;
                        }

                        spriteBatch.Draw(tex, destinationRectangle, sourceRectangle,
                            ent.DrawComponent.TintColor, ent.DrawComponent.Rotation,
                            ent.DrawComponent.Origin, ent.DrawComponent.Flip,
                            ent.DrawComponent.Layer);
                    }
                    spriteBatch.End();
                }
            }
        }
        #endregion

        #region Private Functions
        /* ----------------------------------------------------------------------------------
         * ● FrameStep() runs at an arbitrarily defined "sample rate" to define the speed
         * of an animation
         * ● The logic is very similar to the main fixedupdate loop in CrystalBusters.cs
         * ● TODO: Refactor to use gameTime instead of my own DELTA_TIME and FixedUpdate
         * ---------------------------------------------------------------------------------- */

        private static void FrameStep(Entity ent)
        {
            ent.DrawComponent.CurrentSprite.Accumulator +=
                ent.DrawComponent.CurrentSprite.SampleRate;

            // Step through frames based on SampleRate
            while (ent.DrawComponent.CurrentSprite.Accumulator >= DELTA_TIME) {
                ent.DrawComponent.CurrentSprite.CurrentFrame++; // Go to next frame

                // Clamp CurrentFrame to TotalFrames - This should never be needed outside of test code, but if you update 
                // TotalFrames outside of FixedUpdate, it can start increasing dramatically, and CurrentFrame will never catch up to reset.
                // This "fix" prevents sprites from ever outright disappearing, instead causing them to flicker.
                ent.DrawComponent.CurrentSprite.CurrentFrame =
                    LyricalHelper.ClampInt(ent.DrawComponent.CurrentSprite.CurrentFrame, 0,
                        ent.DrawComponent.CurrentSprite.FrameRange);

                if (ent.DrawComponent.CurrentSprite.CurrentFrame ==
                    ent.DrawComponent.CurrentSprite.FrameRange) {
                    ent.DrawComponent.CurrentSprite.CurrentFrame = 0;
                }

                ent.DrawComponent.CurrentSprite.Accumulator -= DELTA_TIME;
            }
        }

        // Initialize an array of "ParallaxLayers", which themselves hold Entities
        private void SetParallaxLayerArrayLength(int paralayerCount)
        {
            _parallaxLayersArray = new List<Entity>[paralayerCount];

            for (int i = 0; i < _parallaxLayersArray.Length; i++) {
                _parallaxLayersArray[i] = new List<Entity>();
            }
        }

        // Organize entities in the correct ParallaxLayer bin
        private void AddEntitiesToParallaxLayers(Entity ent)
        {
            if (_parallaxLayersArray != null) {
                _parallaxLayersArray[ent.DrawComponent.ParallaxLayer].Add(ent);
            }
        }
        #endregion
    }
}