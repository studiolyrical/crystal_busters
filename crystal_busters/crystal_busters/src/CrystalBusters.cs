﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using Squid;
using VosSoft.Xna.GameConsole;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;
#endregion

namespace crystal_busters
{
    public class CrystalBusters : Game
    {
        #region Default Constructor
        public CrystalBusters()
        {
            var ini = new INIParser("config.ini");

            _graphics = new GraphicsDeviceManager(this) {
                PreferredBackBufferWidth = ini.ReadInt("ScreenWidth", "Graphics", 1280),
                PreferredBackBufferHeight = ini.ReadInt("ScreenHeight", "Graphics", 720),
                PreferMultiSampling = ini.ReadBool("PreferMultiSampling", "Graphics", true),
                SynchronizeWithVerticalRetrace = ini.ReadBool("vsync", "Graphics")
            };

            _devConsole = new GameConsole(this) {
                LogDebugMessages = true, OpeningAnimation = GameConsoleAnimation.None,
                ClosingAnimation = GameConsoleAnimation.None, MaxLogEntries = 10000
            };
        }
        #endregion

        #region Fields
        private readonly GraphicsDeviceManager _graphics;
        private readonly GameConsole _devConsole;
        private SpriteBatch _spriteBatch;
        private Dictionary<string, Texture2D> _textureContent;
        private Dictionary<string, SpriteFont> _fontContent;
        private InputHandler _inputHandler;
        private SceneManager _sceneManager;
        private static bool _quit;

        // Fields for Fixed timestep loop
        private double _accumulator;
        private const double DELTA_TIME = 5;
        #endregion

        #region Initialization
        /* ----------------------------------------------------------------------------------
         * ● Allows the game to perform any initialization it needs to before starting to run.
         * This is where it can query for any required services and load any non-graphic
         * related content.  Calling base.Initialize will enumerate through any components
         * and initialize them as well.
         * ---------------------------------------------------------------------------------- */

        protected override void Initialize()
        {
            IsMouseVisible = false;
            IsFixedTimeStep = false;
            Componenzts.Add(new FrameRateCounter(this, new Vector2(25, 25), Color.White, Color.Black));
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            GameServices.AddService(_graphics);
            GameServices.AddService(_devConsole);
            _inputHandler = new InputHandler();
            GameServices.AddService<ILyricalInputHandlerService>(_inputHandler);
            _sceneManager = new SceneManager();
            GameServices.AddService<ILyricalSceneManagerService>(_sceneManager);
            var lyricalDebug = new LyricalDebug();
            GameServices.AddService<ILyricalDebugService>(lyricalDebug);
            _quit = false;
            _accumulator = 0;

            _sceneManager.AddScene(
                new MainMenuScene(
                    JsonConvert.DeserializeObject<MainMenuScene>(
                        File.ReadAllText("data/mainmenu.lyr"))));

            _devConsole.Initialized += InitializeConsole;

            base.Initialize();
        }

        private void InitializeConsole(object sender, EventArgs e)
        {
            _devConsole.Lines = 20;
            _devConsole.SetLogLevelColor(255, Color.Pink);
            _devConsole.SetLogLevelColor(1, Color.MediumVioletRed);

            var ini = new INIParser("config.ini");

            _devConsole.AddCommand("tog_vsync", delegate {
                _graphics.SynchronizeWithVerticalRetrace = !ini.ReadBool("vsync", "Graphics");
                ini.WriteBool("vsync", !ini.ReadBool("vsync", "Graphics"), "Graphics");
                _graphics.ApplyChanges();
                LyricalDebug.Log(
                    string.Format("vsync on: " + _graphics.SynchronizeWithVerticalRetrace));
            }, "Toggle synchronization with vertical retrace");
        }

        /* ----------------------------------------------------------------------------------
         * ● LoadContent will be called once per game and is the place to load all of your 
         * content.
         * ---------------------------------------------------------------------------------- */

        protected override void LoadContent()
        {
            // TODO: use this.Content to load your game content here
            Content.RootDirectory = "Content";
            _textureContent = Content.LoadListContent<Texture2D>("tex");
            GameServices.AddService(_textureContent);
            _fontContent = Content.LoadListContent<SpriteFont>("font");
            GameServices.AddService(_fontContent);

            Gui.Renderer = new GUIRenderer(this);
        }

        /* ----------------------------------------------------------------------------------
         * ● UnloadContent will be called once per game and is the place to unload all content
         * ---------------------------------------------------------------------------------- */

        protected override void UnloadContent()
        {
            Content.Unload();
        }
        #endregion

        #region Update Loops
        /* ----------------------------------------------------------------------------------
         * ● Update is run as fast as possible since IsFixedTimeStep = false;
         * ● <param name="gameTime">Provides a snapshot of timing values.</param>
         * ---------------------------------------------------------------------------------- */

        protected override void Update(GameTime gameTime)
        {
            Gui.TimeElapsed = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            _inputHandler.Update();
            _sceneManager.Update(gameTime, _inputHandler);
            _inputHandler.ResetState();
            /* ----------------------------------------------------------------------------------
             * ● Below while loop is the "FixedUpdate" logic:
             * ● http://gafferongames.com/game-physics/fix-your-timestep/
             * ---------------------------------------------------------------------------------- */
            _accumulator += gameTime.ElapsedGameTime.TotalMilliseconds;
            while (_accumulator >= DELTA_TIME) {
                _sceneManager.FixedUpdate();
                _accumulator -= DELTA_TIME;
            }

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState(PlayerIndex.One).IsKeyDown(Keys.Escape) || _quit) {
                Exit();
            }

            base.Update(gameTime);
        }

        /* ----------------------------------------------------------------------------------
         * ● This is called when the game should draw itself (usually after every Update()).
         * ● More about calling update/draw here: 
         * http://blogs.msdn.com/b/shawnhar/archive/2007/11/23/game-timing-in-xna-game-studio-2-0.aspx
         * ---------------------------------------------------------------------------------- */

        protected override void Draw(GameTime gameTime)
        {
            _sceneManager.Draw(gameTime, GraphicsDevice, _spriteBatch, _textureContent, _fontContent);

            base.Draw(gameTime);
        }
        #endregion
    }
}