﻿#region Using Statements
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace crystal_busters
{
    public interface ILyricalSceneManagerService
    {
        void AddScene(Scene scn);
    }

    public interface ILyricalInputHandlerService
    {
        KeyboardState KBState { get; }
        MouseState MouseState { get; }
        bool PressOnceKB(Keys key);
        bool PressedMouse(string mouseButton);
        bool PressOnceMouse(string mouseButton);
        Vector2 GetMousePosition(Camera cam = null);
    }

    public interface ILyricalDebugService
    {
#if DEBUG
        void DebugUpdate(ILyricalInputHandlerService inputHandler, List<Entity> entities,
            Camera scnCamera);

        void DebugDrawBottom(SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            MapScene mapScn, List<Rectangle> cells);

        void DebugDrawTop(SpriteBatch spriteBatch, Dictionary<string, Texture2D> textureContent,
            Dictionary<string, SpriteFont> fontContent, MapScene mapScn, List<Entity> entities);

        void PrintEntities(List<Entity> entities, bool withComponents = false,
            bool withProperties = false, bool osd = false);

        void PrintEntities(List<Entity> entities, int componentID);

        void PrintScenes(List<Scene> activeScenes, bool withProperties = false);

        void DrawBorder(SpriteBatch spriteBatch, Camera sceneCamera, Rectangle r, Color color,
            int bw, float layer);
#endif
    }

    public interface ILyricalEventQueueService
    {
        void SendEvent();
        void ReceiveEvent();
    }

    public interface ILyricalComponent
    {
        int InstanceID { get; set; }
    }

    public interface ILyricalComponentSystem {}
}