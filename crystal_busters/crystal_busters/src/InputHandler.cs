﻿#region Using Statements
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Squid;
using VosSoft.Xna.GameConsole;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;
#endregion

namespace crystal_busters
{
    public class InputHandler : ILyricalInputHandlerService
    {
        #region Default Constructor
        public InputHandler()
        {
            LyricalDebug.Assert(!_instantiated);
            KBState = new KeyboardState();
            OldKBState = new KeyboardState();
            MouseState = new MouseState();
            OldMouseState = new MouseState();
            _instantiated = true;
        }
        #endregion

        #region Fields
        private static bool _instantiated;
        private KeyboardState _oldK;
        private int _oldMScroll;
        #endregion

        #region Properties
        public KeyboardState KBState { get; private set; }
        public KeyboardState OldKBState { get; private set; }
        public MouseState MouseState { get; private set; }
        public MouseState OldMouseState { get; private set; }
        #endregion

        #region Update Loops
        public void Update()
        {
            // Mouse
            MouseState = Mouse.GetState();
            int mwheel = MouseState.ScrollWheelValue > _oldMScroll
                ? -1 : (MouseState.ScrollWheelValue < _oldMScroll ? 1 : 0);
            _oldMScroll = MouseState.ScrollWheelValue;

            Gui.SetMouse(MouseState.X, MouseState.Y, mwheel);
            Gui.SetButtons(MouseState.LeftButton == ButtonState.Pressed,
                MouseState.RightButton == ButtonState.Pressed);

            // KB
            var devConsole = GameServices.GetService<GameConsole>();

            KeyboardState k = Keyboard.GetState();

            // Toggle devconsole input:
            if (_oldK.IsKeyDown(Keys.F2) && k.IsKeyUp(Keys.F2)) {
                devConsole.InputEnabled = !devConsole.InputEnabled;
            }

            _oldK = k;

            // don't accept input anymore if input is going to devconsole
            if (devConsole.IsOpen && devConsole.InputEnabled) {
                return;
            }

            KBState = Keyboard.GetState();

            // open devconsole
            if (PressOnceKB(Keys.F1)) {
                devConsole.Open(Keys.F1);
                devConsole.InputEnabled = true;
            }
        }
        #endregion

        #region Interface Functions
        public bool PressOnceKB(Keys key)
        {
            bool isPressed;

            if (OldKBState.IsKeyUp(key) && KBState.IsKeyDown(key)) {
                isPressed = true;
            } else {
                isPressed = false;
            }

            return isPressed;
        }

        public bool PressedMouse(string mouseButton)
        {
            bool isPressed = false;

            switch (mouseButton) {
                case "LeftButton":
                    isPressed = OldMouseState.LeftButton == ButtonState.Pressed &&
                                MouseState.LeftButton == ButtonState.Pressed ? true : false;
                    break;
                case "RightButton":
                    isPressed = OldMouseState.RightButton == ButtonState.Pressed &&
                                MouseState.RightButton == ButtonState.Pressed ? true : false;
                    break;
                case "MiddleButton":
                    isPressed = OldMouseState.MiddleButton == ButtonState.Pressed &&
                                MouseState.MiddleButton == ButtonState.Pressed ? true : false;
                    break;
                case "XButton1":
                    isPressed = OldMouseState.XButton1 == ButtonState.Pressed &&
                                MouseState.XButton1 == ButtonState.Pressed ? true : false;
                    break;
                case "XButton2":
                    isPressed = OldMouseState.XButton2 == ButtonState.Pressed &&
                                MouseState.XButton2 == ButtonState.Pressed ? true : false;
                    break;
                default:
                    LyricalDebug.ExceptionMessage(
                        "You're trying to use a MouseButton that doesn't exist.\n\nValue: " +
                        mouseButton +
                        "\nValid values: LeftButton, RightButton, MiddleButton, XButton1, XButton2");
                    Debugger.Break();
                    break;
            }

            return isPressed;
        }

        public bool PressOnceMouse(string mouseButton)
        {
            bool isPressed = false;

            switch (mouseButton) {
                case "LeftButton":
                    isPressed = OldMouseState.LeftButton == ButtonState.Released &&
                                MouseState.LeftButton == ButtonState.Pressed ? true : false;
                    break;
                case "RightButton":
                    isPressed = OldMouseState.RightButton == ButtonState.Released &&
                                MouseState.RightButton == ButtonState.Pressed ? true : false;
                    break;
                case "MiddleButton":
                    isPressed = OldMouseState.MiddleButton == ButtonState.Released &&
                                MouseState.MiddleButton == ButtonState.Pressed ? true : false;
                    break;
                case "XButton1":
                    isPressed = OldMouseState.XButton1 == ButtonState.Released &&
                                MouseState.XButton1 == ButtonState.Pressed ? true : false;
                    break;
                case "XButton2":
                    isPressed = OldMouseState.XButton2 == ButtonState.Released &&
                                MouseState.XButton2 == ButtonState.Pressed ? true : false;
                    break;
                default:
                    LyricalDebug.ExceptionMessage(
                        "You're trying to use a MouseButton that doesn't exist.\n\nValue: " +
                        mouseButton +
                        "\nValid values: LeftButton, RightButton, MiddleButton, XButton1, XButton2");
                    break;
            }

            return isPressed;
        }

        public Vector2 GetMousePosition(Camera cam = null)
        {
            Vector2 mousePos;

            if (cam != null) { // we looking for world coords?
                mousePos = Vector2.Transform(new Vector2(MouseState.X, MouseState.Y),
                    Matrix.Invert(cam.GetViewMatrix()));
            } else { // or screen?
                mousePos = new Vector2(MouseState.X, MouseState.Y);
            }

            return mousePos;
        }
        #endregion

        #region Public Functions
        // Used in the main game loop
        public void ResetState()
        {
            OldKBState = KBState;
            OldMouseState = MouseState;
        }
        #endregion
    }
}