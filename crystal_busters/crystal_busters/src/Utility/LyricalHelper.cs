﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#endregion

/* ----------------------------------------------------------------------------------
 * Description:
 * ● Utility class containing static functions for various operations. うふふっ！
 * ---------------------------------------------------------------------------------- */

namespace crystal_busters
{
    public static class LyricalHelper
    {
        #region Public Functions
        // XNA MathHelper.Clamp only accepts floats, so this one does ints
        public static int ClampInt(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }

        public static Color ColorFromInt32(int color)
        {
            byte[] bytes = BitConverter.GetBytes(color);
            return new Color(bytes[2], bytes[1], bytes[0], bytes[3]);
        }

        public static Point Vector2ToPoint(Vector2 v)
        {
            var p = new Point((int)v.X, (int)v.Y);
            return p;
        }

        // Loads all content in a single directory
        public static Dictionary<string, T> LoadListContent<T>(this ContentManager contentManager,
            string contentFolder)
        {
            var dir = new DirectoryInfo(contentManager.RootDirectory + "/" + contentFolder);
            if (!dir.Exists) {
                throw new DirectoryNotFoundException();
            }
            var result = new Dictionary<string, T>();

            FileInfo[] files = dir.GetFiles("*.*");
            foreach (FileInfo file in files) {
                string key = Path.GetFileNameWithoutExtension(file.Name);
                result[key] = contentManager.Load<T>(contentFolder + "/" + key);
            }
            return result;
        }
        #endregion
    }
}