﻿#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace crystal_busters
{
    // http://roy-t.nl/index.php/2010/08/25/xna-accessing-contentmanager-and-graphicsdevice-anywhere-anytime-the-gameservicecontainer/
    // TODO: Expand on this to allow null services
    // TODO: maybe make it so we don't need to call get service anymore
    public static class GameServices
    {
        private static GameServiceContainer _container;
        public static GameServiceContainer Instance {
            get { return _container ?? (_container = new GameServiceContainer()); }
        }

        public static T GetService<T>()
        {
            return (T)Instance.GetService(typeof (T));
        }

        public static void AddService<T>(T service)
        {
            Instance.AddService(typeof (T), service);
        }

        public static void RemoveService<T>()
        {
            Instance.RemoveService(typeof (T));
        }
    }
}