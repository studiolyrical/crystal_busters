﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using VosSoft.Xna.GameConsole;
#endregion

namespace crystal_busters
{
    public class LyricalDebug : ILyricalDebugService
    {
        #region Default Constructor
        public LyricalDebug()
        {
            Assert(!_instantiated);
#if DEBUG
            DebugConfig =
                JsonConvert.DeserializeObject<DevConfig>(File.ReadAllText("data/dev/dev_config.lyr"));
            GraphicsDevice gd = GameServices.GetService<GraphicsDeviceManager>().GraphicsDevice;
            _borderTile = new Texture2D(gd, 1, 1, false, SurfaceFormat.Color);
            _borderTile.SetData(new[] {Color.White});
            AddConsoleCommands();
#endif

            _instantiated = true;
        }
        #endregion

        #region Fields
        private readonly Texture2D _borderTile;
        private static bool _instantiated; // Assertion check
        #endregion

        #region Properties
        public static DevConfig DebugConfig { get; private set; }
        #endregion

        #region Public Static Functions
        // Throw a custom exception messagebox
        public static void ExceptionMessage(string s = "")
        {
            var devConsole = GameServices.GetService<GameConsole>();
            if (devConsole.IsOpen) {
                devConsole.Log(s);
            }
            Console.WriteLine(s + "\n\nStack trace:\n" + Environment.StackTrace);
            MessageBox(new IntPtr(0), s + "\n\nStack trace:\n" + Environment.StackTrace,
                "♥リリカル☆マジカル EXCEPTION!♥", 0);
        }

        // Microsoft's Assert() method isn't aggressive enough for me, this one runs Debugger.Break() 
        public static void Assert(bool c)
        {
            if (!c) {
                ExceptionMessage("Assert failed");
                Debugger.Break();
            }
        }

        public static void Log(string str)
        {
            var devConsole = GameServices.GetService<GameConsole>();
            devConsole.Log(str);
            Console.WriteLine(str);
        }
        #endregion

        #region Interface Functions
#if DEBUG
        public void DebugUpdate(ILyricalInputHandlerService inputHandler, List<Entity> entities,
            Camera cam)
        {
            if (DebugConfig.EnableDebugMouse) {
                Vector2 mousePos = inputHandler.GetMousePosition(cam);

                if (inputHandler.PressOnceMouse("MiddleButton")) {
                    Log(
                        string.Format("Mouse X: {0}, Y: {1}", inputHandler.GetMousePosition(cam).X,
                            inputHandler.GetMousePosition(cam).Y));
                }

                foreach (Entity ent in entities) {
                    if (
                        ent.ActorComponent.Hurtbox.Contains(new Point((int)mousePos.X,
                            (int)mousePos.Y))) {
                        if (inputHandler.PressOnceMouse("MiddleButton")) {
                            Log(string.Format("{0} inst ID: {1}", ent.Name, ent.InstanceID));
                        }

                        if (inputHandler.PressedMouse("LeftButton")) {
                            ent.PositionComponent.Position = inputHandler.GetMousePosition(cam);
                        }
                    }
                }
            }
        }

        public void DrawBorder(SpriteBatch spriteBatch, Camera sceneCamera, Vector4 v4, Color color,
            int bw, float layer)
        {
            spriteBatch.Draw(_borderTile, new Rectangle((int)v4.X, (int)v4.Y, (int)v4.Z, bw), null,
                color, 0f, Vector2.Zero, SpriteEffects.None, layer);
            spriteBatch.Draw(_borderTile, new Rectangle((int)v4.X, (int)v4.Y, bw, (int)v4.W), null,
                color, 0f, Vector2.Zero, SpriteEffects.None, layer);
            spriteBatch.Draw(_borderTile,
                new Rectangle((int)v4.X + (int)v4.Z - bw, (int)v4.Y, bw, (int)v4.W), null, color,
                0f, Vector2.Zero, SpriteEffects.None, layer);
            spriteBatch.Draw(_borderTile,
                new Rectangle((int)v4.X, (int)v4.Y + (int)v4.W, (int)v4.Z, bw), null, color, 0f,
                Vector2.Zero, SpriteEffects.None, layer);
        }

        // Drawing debug stuff based on devsettings, this stuff should appear below entities
        public void DebugDrawBottom(SpriteBatch spriteBatch,
            Dictionary<string, Texture2D> textureContent, MapScene mapScn, List<Rectangle> cells)
        {
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend,
                SamplerState.AnisotropicClamp, null, null, null,
                mapScn.SceneCamera.GetViewMatrix(1, 1));

            if (DebugConfig.MainGridVisible) {
                DrawBorder(spriteBatch, mapScn.SceneCamera, mapScn.MainGridRectangleVector4,
                    DebugConfig.MainGridBorderColor, DebugConfig.MainGridBorderWidth, 0.01f);
            }

            if (DebugConfig.MainGridDividerVisible) {
                DrawBorder(spriteBatch, mapScn.SceneCamera,
                    new Vector4(
                        mapScn.MainGridRectangleVector4.X +
                        mapScn.MainGridRectangleVector4.Z * 0.5f,
                        mapScn.MainGridRectangleVector4.Y,
                        0 + DebugConfig.MainGridDividerBorderWidth,
                        mapScn.MainGridRectangleVector4.W - DebugConfig.MainGridDividerBorderWidth),
                    DebugConfig.MainGridDividerColor, DebugConfig.MainGridDividerBorderWidth, 0.02f);
            }

            if (DebugConfig.PlayerGridDividerVisible) {
                DrawBorder(spriteBatch, mapScn.SceneCamera,
                    new Vector4(
                        mapScn.MainGridRectangleVector4.X +
                        mapScn.MainGridRectangleVector4.Z * 0.5f +
                        mapScn.PlayerGridOffset * 0.5f,
                        mapScn.MainGridRectangleVector4.Y +
                        DebugConfig.PlayerGridDividerBorderWidth, 0,
                        mapScn.MainGridRectangleVector4.W -
                        DebugConfig.PlayerGridDividerBorderWidth),
                    DebugConfig.PlayerGridDividerColor, DebugConfig.PlayerGridDividerBorderWidth,
                    0.02f);

                DrawBorder(spriteBatch, mapScn.SceneCamera,
                    new Vector4(
                        mapScn.MainGridRectangleVector4.X +
                        mapScn.MainGridRectangleVector4.Z * 0.5f +
                        -mapScn.PlayerGridOffset * 0.5f,
                        mapScn.MainGridRectangleVector4.Y +
                        DebugConfig.PlayerGridDividerBorderWidth, 0,
                        mapScn.MainGridRectangleVector4.W -
                        DebugConfig.PlayerGridDividerBorderWidth),
                    DebugConfig.PlayerGridDividerColor, DebugConfig.PlayerGridDividerBorderWidth,
                    0.02f);
            }

            if (DebugConfig.PlayerGridVisible) {
                if (cells != null) {
                    for (int i = 0; i < cells.Count; ++i) {
                        Color color = i % 2 == 0
                            ? DebugConfig.PlayerGridColorEven : DebugConfig.PlayerGridColorOdd;

                        DrawBorder(spriteBatch, mapScn.SceneCamera, cells[i], color,
                            DebugConfig.PlayerGridDividerBorderWidth, 0.00f);
                    }
                }
            }

            spriteBatch.End();
        }

        // and this stuff should appear on top of entities
        public void DebugDrawTop(SpriteBatch spriteBatch,
            Dictionary<string, Texture2D> textureContent, Dictionary<string, SpriteFont> fontContent,
            MapScene mapScn, List<Entity> entities)
        {
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend,
                SamplerState.AnisotropicClamp, null, null, null,
                mapScn.SceneCamera.GetViewMatrix(1.0f, 1.0f));

            foreach (Entity ent in entities) {
                if (ent.ActorComponent != null) {
                    if (DebugConfig.HurtboxesVisible) {
                        DrawBorder(spriteBatch, mapScn.SceneCamera, ent.ActorComponent.Hurtbox,
                            DebugConfig.HurtboxColor, DebugConfig.HurtboxBorderWidth, 0.0f);
                    }
                }

                if (DebugConfig.EntityDataOnscreen) {
                    int spacing = 0;
                    spriteBatch.DrawString(fontContent["devtextsmall"],
                        ent.InstanceID + " " + ent.Name,
                        new Vector2(ent.PositionComponent.Position.X + 20,
                            ent.PositionComponent.Position.Y + -60 + spacing), Color.White);

                    IEnumerable<ILyricalComponent> componentList = CheckEntityComponents(ent);
                    foreach (ILyricalComponent c in componentList) {
                        spacing += 10;
                        string cName =
                            c.GetType().ToString().Remove(0, 16).Replace("EntityComponent", "");
                        spriteBatch.DrawString(fontContent["devtextsmall"],
                            "  " + c.InstanceID + " " + cName,
                            new Vector2(ent.PositionComponent.Position.X + 20,
                                ent.PositionComponent.Position.Y + -60 + spacing), Color.White);
                    }
                }
            }
            spriteBatch.End();
        }

        // Draw stuff like hurtboxes, hitboxes, any border
        public void DrawBorder(SpriteBatch spriteBatch, Camera sceneCamera, Rectangle r, Color color,
            int bw, float layer)
        {
            spriteBatch.Draw(_borderTile, new Rectangle(r.X, r.Y, r.Width, bw), null, color, 0f,
                Vector2.Zero, SpriteEffects.None, layer);
            spriteBatch.Draw(_borderTile, new Rectangle(r.X, r.Y, bw, r.Height), null, color, 0f,
                Vector2.Zero, SpriteEffects.None, layer);
            spriteBatch.Draw(_borderTile, new Rectangle(r.X + r.Width - bw, r.Y, bw, r.Height),
                null, color, 0f, Vector2.Zero, SpriteEffects.None, layer);
            spriteBatch.Draw(_borderTile, new Rectangle(r.X, r.Y + r.Height, r.Width, bw), null,
                color, 0f, Vector2.Zero, SpriteEffects.None, layer);
        }

        // REFLECTION, set with components true to do a shallow print of all their componenets too
        public void PrintEntities(List<Entity> entities, bool withComponents = false,
            bool withProperties = false, bool osd = false)
        {
            if (withComponents) {
                Log("\n\n＊～ENTITIES & THEIR COMPONENTS～＊\n");

                foreach (Entity ent in entities) {
                    Log("★Entity Name: " + ent.Name + " // Instance ID: " + ent.InstanceID);

                    IEnumerable<ILyricalComponent> componentList = CheckEntityComponents(ent);

                    foreach (ILyricalComponent component in componentList) {
                        Log("   ☆" + component + " // Instance ID: " + component.InstanceID);

                        if (withProperties) {
                            foreach (PropertyInfo p in
                                component.GetType().GetProperties().Where(
                                    p => !p.GetGetMethod().GetParameters().Any())) {
                                Log("     -" + p.Name + ": " + p.GetValue(component, null));
                            }
                        }
                    }
                }
            } else {
                Log("\n\nENTITIES\n");

                foreach (Entity ent in entities) {
                    Log(ent.InstanceID + " " + ent.Name + " " + ent.PositionComponent.Position);
                }
            }
        }

        public void PrintEntities(List<Entity> entities, int componentID)
        {
            Log("\n\n＊～ENTITIES & THEIR COMPONENTS～＊\n");

            foreach (Entity ent in entities) {
                Log("★Entity Name: " + ent.Name + " // Instance ID: " + ent.InstanceID);

                IEnumerable<ILyricalComponent> componentList = CheckEntityComponents(ent);

                foreach (ILyricalComponent component in componentList) {
                    if (component.InstanceID == componentID) {
                        Log("   ☆" + component + " // Instance ID: " + component.InstanceID);
                        foreach (PropertyInfo p in
                            component.GetType().GetProperties().Where(
                                p => !p.GetGetMethod().GetParameters().Any())) {
                            Log("     -" + p.Name + ": " + p.GetValue(component, null));
                        }
                    }
                }
            }
        }

        public void PrintScenes(List<Scene> activeScenes, bool withProperties = false)
        {
            Log("\n\n＊～ACTIVE SCENES～＊\n");

            foreach (Scene scn in activeScenes) {
                Log("★Scene Name: " + scn.Name + " // Instance ID: " + scn.InstanceID);
                if (withProperties) {
                    foreach (PropertyInfo p in
                        scn.GetType().GetProperties().Where(
                            p => !p.GetGetMethod().GetParameters().Any())) {
                        Log("   ☆" + p.Name + ": " + p.GetValue(scn, null));
                    }
                }
            }
        }

        public static IEnumerable<ILyricalComponent> CheckEntityComponents(Entity ent)
        {
            var componentList = new List<ILyricalComponent>();

            // Might be worth considering giving entities a list property that holds all of their components, but for now just duping
            // the null checks here.
            if (ent.PositionComponent != null) {
                componentList.Add(ent.PositionComponent);
            }
            if (ent.DrawComponent != null) {
                componentList.Add(ent.DrawComponent);
            }
            if (ent.ActorComponent != null) {
                componentList.Add(ent.ActorComponent);
            }
            if (ent.UnitComponent != null) {
                componentList.Add(ent.UnitComponent);
            }

            return componentList;
        }
#endif

        #region Private Functions
        // Grab user32.dll and hit up some message boxes
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern uint MessageBox(IntPtr hWnd, string text, string caption, uint type);

#if DEBUG

        private static void AddConsoleCommands()
        {
            var devConsole = GameServices.GetService<GameConsole>();

            devConsole.AddCommand("debug_tog_mouse", delegate {
                DebugConfig.EnableDebugMouse = !DebugConfig.EnableDebugMouse;
                Log("Debug mouse " + DebugConfig.EnableDebugMouse);
            }, "Enable/disable dev helper mouse stuff");

            devConsole.AddCommand("debug_tog_osd", delegate {
                DebugConfig.EntityDataOnscreen = !DebugConfig.EntityDataOnscreen;
                Log("Entity Component OSD " + DebugConfig.EntityDataOnscreen);
            });
        }
#endif
        #endregion
    }
    #endregion
}