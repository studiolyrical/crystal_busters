﻿#region Using Statements
using System;
using Microsoft.Xna.Framework;
#endregion

/* ----------------------------------------------------------------------------------
 * Description:
 * ● Utility class to display FPS 
 * ● http://blogs.msdn.com/b/shawnhar/archive/2007/06/08/displaying-the-framerate.aspx 
 * ● atm this is added as a component
 * 
 * TODO: 
 * ● Right now a lot of stuff is commented out from this class, and all of 
 * that stuff relates to drawing the FPS on-screen. I'll want a better "OSD" later 
 * when I write devconsole & toggleable information, but for now I want FPS to 
 * display in titlebar.
 * ---------------------------------------------------------------------------------- */

namespace crystal_busters
{
    public class FrameRateCounter : DrawableGameComponent
    {
        //public Vector2 Position { get; set; }
        //public Vector2 Position2 { get; set; }
        //public Color Color { get; set; }
        //public Color Color2 { get; set; }

        //private ContentManager content;
        //private SpriteBatch spriteBatch;
        //private SpriteFont spriteFont;

        private int _frameRate;
        private int _frameCounter;
        private TimeSpan _elapsedTime = TimeSpan.Zero;

        public FrameRateCounter(Game game, Vector2 position, Color color, Color color2)
            : base(game)
        {
            //content = game.Content;
            //Position = position;
            //Position2 = new Vector2(Position.X + 1, Position.Y + 1);
            //Color = color;
            //Color2 = color2;
        }

        //protected override void LoadContent()
        //{
        //    spriteBatch = new SpriteBatch(GraphicsDevice);
        //    spriteFont = content.Load<SpriteFont>("Fonts/fpsCounter");
        //}

        //protected override void UnloadContent()
        //{
        //    content.Unload();
        //}

        public override void Update(GameTime gameTime)
        {
            _elapsedTime += gameTime.ElapsedGameTime;

            if (_elapsedTime > TimeSpan.FromSeconds(1)) {
                _elapsedTime -= TimeSpan.FromSeconds(1);
                _frameRate = _frameCounter;
                _frameCounter = 0;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _frameCounter++;
            Game.Window.Title = "CRYSTAL BUSTERS || fps: " + _frameRate;

            //string text = "fps: " + frameRate;

            //spriteBatch.Begin();
            //spriteBatch.DrawString(spriteFont, text, Position2, Color2);
            //spriteBatch.DrawString(spriteFont, text, Position, Color);
            //spriteBatch.End();
        }
    }
}