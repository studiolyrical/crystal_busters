﻿#region Using Statements
using Microsoft.Xna.Framework;
#endregion

namespace crystal_busters
{
    // This is temporary configuration for during development.
    // Any real config that should be included in release builds, should go into
    // config.ini. This is just a lazymode class for some debug stuffs.
    public class DevConfig
    {
        #region Default Constructor
        public DevConfig() {}
        #endregion

        #region Properties
        public bool BackgroundVisible { get; set; }

        public bool HurtboxesVisible { get; set; }
        public int HurtboxBorderWidth { get; set; }
        public Color HurtboxColor { get; set; }

        public bool MainGridVisible { get; set; }
        public Color MainGridBorderColor { get; set; }
        public int MainGridBorderWidth { get; set; }

        public bool MainGridDividerVisible { get; set; }
        public Color MainGridDividerColor { get; set; }
        public int MainGridDividerBorderWidth { get; set; }

        public bool PlayerGridVisible { get; set; }
        public Color PlayerGridColorEven { get; set; }
        public Color PlayerGridColorOdd { get; set; }
        public int PlayerGridBorderWidth { get; set; }

        public bool PlayerGridDividerVisible { get; set; }
        public Color PlayerGridDividerColor { get; set; }
        public int PlayerGridDividerBorderWidth { get; set; }

        public string MapDataLocation { get; set; }
        public int LastMapSaveID { get; set; }

        public bool EntityDataOnscreen { get; set; }

        public bool EnableDebugMouse { get; set; }
        #endregion
    }
}