﻿#region Using Statements
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
#endregion

namespace crystal_busters
{
    public class INIParser
    {
        public INIParser(string iniPath)
        {
            _path = new FileInfo(iniPath).FullName;
        }

        private readonly string _path;
        private const string DEF_SECTION = "crystal_busters";

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key,
            string value, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string Default,
            StringBuilder retVal, int size, string filePath);


        public string ReadString(string key, string section = null, string defVal = "")
        {
            var retVal = new StringBuilder(255);
            GetPrivateProfileString(section ?? DEF_SECTION, key, defVal, retVal, 255, _path);
            return retVal.ToString();
        }

        public int ReadInt(string key, string section = null, int defVal = 0)
        {
            var retValSb = new StringBuilder(255);
            GetPrivateProfileString(section ?? DEF_SECTION, key, defVal.ToString(), retValSb, 255,
                _path);
            int retVal;
            try {
                retVal = int.Parse(retValSb.ToString());
            } catch (Exception) {
                Console.WriteLine(
                    "Could not read value for {0} from {1}. Resetting value to default of {2}.\nSupplied value: {3}",
                    key, _path, defVal, retValSb);
                retVal = defVal;
            }
            return retVal;
        }

        public bool ReadBool(string key, string section = null, bool defVal = false)
        {
            var retValSb = new StringBuilder(255);
            GetPrivateProfileString(section ?? DEF_SECTION, key, defVal.ToString(), retValSb, 255,
                _path);
            bool retVal;
            try {
                retVal = bool.Parse(retValSb.ToString());
            } catch (Exception) {
                Console.WriteLine(
                    "Could not read value for {0} from {1}. Resetting value to default of {2}.\nSupplied value: {3}",
                    key, _path, defVal, retValSb);
                retVal = defVal;
            }
            return retVal;
        }

        public void WriteString(string key, string value, string section = null)
        {
            WritePrivateProfileString(section ?? DEF_SECTION, key, value, _path);
        }

        public void WriteInt(string key, int value, string section = null)
        {
            WritePrivateProfileString(section ?? DEF_SECTION, key, value.ToString(), _path);
        }

        public void WriteBool(string key, bool value, string section = null)
        {
            WritePrivateProfileString(section ?? DEF_SECTION, key, value.ToString(), _path);
        }

        public void DeleteKey(string key, string section = null)
        {
            WriteString(key, null, section ?? DEF_SECTION);
        }

        public void DeleteSection(string section = null)
        {
            WriteString(null, null, section ?? DEF_SECTION);
        }

        public bool KeyExists(string key, string section = null)
        {
            return ReadString(key, section).Length > 0;
        }
    }
}